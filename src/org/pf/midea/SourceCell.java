/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class SourceCell
{
    private PlanStates.SourceType sourceType;

    public SourceCell(PlanStates.SourceType _sourceType)
    {
        sourceType = _sourceType;
    }

    public PlanStates.SourceType getSourceType()
    {
        return sourceType;
    }

    public String getSourceTypeName()
    {
        String out = null;
        switch (sourceType)
        {
            case ST_TEST:
                out = "Тестові послідовності";
                break;
            case ST_RANDOM:
                out = "ГПВЧ";
                break;
        }
        return out;
    }
}
