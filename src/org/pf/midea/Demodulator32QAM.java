/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.RecursiveTask;

public class Demodulator32QAM extends RecursiveTask<BinaryNumber[]> implements Demodulator
{
    public static final double BOUND2 = 2 / (5 * Math.sqrt(2));
    public static final double BOUND4 = 4 / (5 * Math.sqrt(2));

    private Signal[] signals;
    private int lowBound, highBound, sequentialThreshold;

    public Demodulator32QAM()
    {

    }

    public Demodulator32QAM(Signal[] _signals, int _lowBound, int _highBound, int _sequentialThreshold)
    {
        signals = _signals;
        lowBound = _lowBound;
        highBound = _highBound;
        sequentialThreshold = _sequentialThreshold;
    }

    @Override
    public BinaryNumber[] compute()
    {
        BinaryNumber[] out = new BinaryNumber[highBound - lowBound];
        if (highBound - lowBound <= sequentialThreshold)
        {
            for (int index = lowBound; index < highBound; ++index)
            {
                double realI = signals[index].getI(), realQ = signals[index].getQ();
                double foundI = 0, foundQ = 0;
                if (realI <= -BOUND4)
                    foundI = -5;
                else if (realI >= -BOUND4 && realI <= -BOUND2)
                    foundI = -3;
                else if (realI >= -BOUND2 && realI <= 0)
                    foundI = -1;
                else if (realI >= 0 && realI <= BOUND2)
                    foundI = 1;
                else if (realI >= BOUND2 && realI <= BOUND4)
                    foundI = 3;
                else if (realI >= BOUND4)
                    foundI = 5;

                if (realQ <= -BOUND4)
                    foundQ = -5;
                else if (realQ >= -BOUND4 && realQ <= -BOUND2)
                    foundQ = -3;
                else if (realQ >= -BOUND2 && realQ <= 0)
                    foundQ = -1;
                else if (realQ >= 0 && realQ <= BOUND2)
                    foundQ = 1;
                else if (realQ >= BOUND2 && realQ <= BOUND4)
                    foundQ = 3;
                else if (realQ >= BOUND4)
                    foundQ = 5;

                if (foundI == 5 && foundQ == 5)
                {
                    double one = Math.sqrt(Math.pow(5 - foundI, 2) + Math.pow(3 - foundQ, 2));
                    double two = Math.sqrt(Math.pow(3 - foundI, 2) + Math.pow(5 - foundQ, 2));
                    if (one < two)
                        foundQ = 3;
                    else
                        foundI = 3;
                }
                else if (foundI == 5 && foundQ == -5)
                {
                    double one = Math.sqrt(Math.pow(5 - foundI, 2) + Math.pow(-3 - foundQ, 2));
                    double two = Math.sqrt(Math.pow(3 - foundI, 2) + Math.pow(-5 - foundQ, 2));
                    if (one < two)
                        foundQ = -3;
                    else
                        foundI = 3;
                }
                else if (foundI == -5 && foundQ == 5)
                {
                    double one = Math.sqrt(Math.pow(-5 - foundI, 2) + Math.pow(3 - foundQ, 2));
                    double two = Math.sqrt(Math.pow(-3 - foundI, 2) + Math.pow(5 - foundQ, 2));
                    if (one < two)
                        foundQ = 3;
                    else
                        foundI = -3;
                }
                else if (foundI == -5 && foundQ == -5)
                {
                    double one = Math.sqrt(Math.pow(-5 - foundI, 2) + Math.pow(-3 - foundQ, 2));
                    double two = Math.sqrt(Math.pow(-3 - foundI, 2) + Math.pow(-5 - foundQ, 2));
                    if (one < two)
                        foundQ = -3;
                    else
                        foundI = -3;
                }

                BinaryNumber code = null;
                for (ConstellationPoint ccp: Constellations.mapGray32QAM)
                    if (ccp.getI() == foundI && ccp.getQ() == foundQ)
                    {
                        code = ccp.getCode();
                        break;
                    }
                out[index - lowBound] = code;
            }
        } else
        {
            int mid = lowBound + (highBound - lowBound) / 2;
            Demodulator32QAM left  = new Demodulator32QAM(signals, lowBound, mid, sequentialThreshold);
            Demodulator32QAM right = new Demodulator32QAM(signals, mid, highBound, sequentialThreshold);
            left.fork();
            BinaryNumber[] rightAns = right.compute();
            BinaryNumber[] leftAns  = left.join();
            System.arraycopy(leftAns, 0, out, 0, leftAns.length);
            System.arraycopy(rightAns, 0, out, leftAns.length, rightAns.length);
            rightAns = null;
            leftAns = null;
        }
        return out;
    }

    public BinaryNumber[] demodulate(Signal[] _array)
    {
        return WorkersTools.fjPool.invoke(new Demodulator32QAM(_array, 0, _array.length, WorkersTools.getSequentialThreshold(_array.length)));
    }
}
