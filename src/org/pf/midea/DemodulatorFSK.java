/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.RecursiveTask;

public class DemodulatorFSK extends RecursiveTask<BinaryNumber[]> implements Demodulator
{
	private final double STEP = Math.PI / 4;
	private Signal[] signals;
	private int lowBound, highBound, sequentialThreshold;

	public DemodulatorFSK()
	{

	}

	public DemodulatorFSK(Signal[] _signals, int _lowBound, int _highBound, int _sequentialThreshold)
	{
		signals = _signals;
		lowBound = _lowBound;
		highBound = _highBound;
		sequentialThreshold = _sequentialThreshold;
	}

	@Override
	public BinaryNumber[] compute()
	{
		BinaryNumber[] out = new BinaryNumber[highBound - lowBound];
		if (highBound - lowBound <= sequentialThreshold)
		{
			for (int index = lowBound; index < highBound; ++index)
			{
				double realI = signals[index].getI(), realQ = signals[index].getQ();
				double angle = Math.atan2(realQ, realI);
				if (angle < 0)
					angle = 2.0 * Math.PI + angle;

				BinaryNumber code = null;
				double foundAngle;
				if (angle >= STEP && angle <= 5 * STEP)
					foundAngle = 2 * STEP;
				else
					foundAngle = 0;
				for (ConstellationPoint ccp: Constellations.mapFSK)
					if (ccp.getAngle() == foundAngle)
					{
						code = ccp.getCode();
						break;
					}
				out[index - lowBound] = code;
			}
		} else
		{
			int mid = lowBound + (highBound - lowBound) / 2;
			DemodulatorFSK left  = new DemodulatorFSK(signals, lowBound, mid, sequentialThreshold);
			DemodulatorFSK right = new DemodulatorFSK(signals, mid, highBound, sequentialThreshold);
			left.fork();
			BinaryNumber[] rightAns = right.compute();
			BinaryNumber[] leftAns  = left.join();
			System.arraycopy(leftAns, 0, out, 0, leftAns.length);
			System.arraycopy(rightAns, 0, out, leftAns.length, rightAns.length);
			rightAns = null;
			leftAns = null;
		}
		return out;
	}

	public BinaryNumber[] demodulate(Signal[] _array)
	{
		return WorkersTools.fjPool.invoke(new DemodulatorFSK(_array, 0, _array.length, WorkersTools.getSequentialThreshold(_array.length)));
	}
}
