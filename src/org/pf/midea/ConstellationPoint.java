/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class ConstellationPoint
{
	private BinaryNumber code;
	private double I, Q, angle;
	
	public ConstellationPoint(BinaryNumber _code, double _I, double _Q)
	{
		code = _code;
		I = _I;
		Q = _Q;
		angle = Math.atan2(_Q, _I);
	}
	
	public ConstellationPoint(BinaryNumber _code, double _angle)
	{
		code = _code;
		angle = _angle;
		I = Math.cos(angle);
		Q = Math.sin(angle);
	}
	
	public BinaryNumber getCode()
	{
		return code;
	}
	
	public double getI()
	{
		return I;
	}

	public double getQ()
	{
		return Q;
	}

	public double getAngle()
	{
		return angle;
	}
}
