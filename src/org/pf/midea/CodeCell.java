/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class CodeCell
{
    private PlanStates.CodeType codeType;

    public CodeCell(PlanStates.CodeType _codeType)
    {
        codeType = _codeType;
    }

    public PlanStates.CodeType getCodeType()
    {
        return codeType;
    }

    public String getCodeTypeName()
    {
        String out = null;
        switch (codeType)
        {
            case CT_NONE:
                out = "Без кодування";
                break;
            case CT_HAMMING74:
                out = "Код Хемінга (7, 4)";
                break;
            case CT_CYCLIC:
                out = "Циклічний код (8, 5)";
                break;
            case CT_BCH155:
                out = "Код БЧХ (15, 5, 7)";
                break;
        }
        return out;
    }
}
