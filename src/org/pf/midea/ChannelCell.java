/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class ChannelCell
{
    private PlanStates.ChannelType channelType;

    public ChannelCell(PlanStates.ChannelType _channelType)
    {
        channelType = _channelType;
    }

    public PlanStates.ChannelType getChannelType()
    {
        return channelType;
    }

    public String getChannelTypeName()
    {
        String out = null;
        switch (channelType)
        {
            case CHT_AWGN:
                out = "АБГШ";
                break;
            case CHT_RAYLEIGH:
                out = "Релея";
                break;
        }
        return out;
    }
}
