/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.RecursiveTask;

public class ChannelRayleigh extends RecursiveTask<Signal[]> implements Channel
{
	private final double sigma1 = Math.sqrt(1 / 2.0);
	private double SNR, sigma2;
	private Signal[] signals;
	private int lowBound, highBound, sequentialThreshold;

	public ChannelRayleigh()
	{

	}

	private ChannelRayleigh(double _SNR, Signal[] _signals, int _lowBound, int _highBound, int _sequentialThreshold)
	{
		SNR = _SNR;
		sigma2 = Math.sqrt(1 / (2.0 * SNR));
		signals = _signals;
		lowBound = _lowBound;
		highBound = _highBound;
		sequentialThreshold = _sequentialThreshold;
	}

	@Override
	protected Signal[] compute()
	{
		Signal[] out = new Signal[highBound - lowBound];
		if (highBound - lowBound <= sequentialThreshold)
		{

			for (int index = lowBound; index < highBound; ++index)
			{
				double h_R = sigma1 * PseudoRNG.nextGaussian();
				double h_I = sigma1 * PseudoRNG.nextGaussian();
				double n_R = sigma2 * PseudoRNG.nextGaussian();
				double n_I = sigma2 * PseudoRNG.nextGaussian();
				double s_R = signals[index].getI();
				double s_I = signals[index].getQ();

				double y_R = h_R * s_R - h_I * s_I + n_R;
				double y_I = h_I * s_R + h_R * s_I + n_I;

				double divisor = Math.pow(h_R, 2) + Math.pow(h_I, 2);
				double dividend_R_1 = y_R * h_R;
				double dividend_R_2 = y_I * h_I;
				double dividend_R;
				if (Double.isInfinite(dividend_R_1) && Double.isInfinite(dividend_R_2))
					dividend_R = dividend_R_1;
				else
					dividend_R = dividend_R_1 + dividend_R_2;
				double dividend_I;
				double dividend_I_1 = y_I * h_R;
				double dividend_I_2 = y_R * h_I;
				if (Double.isInfinite(dividend_I_1) && Double.isInfinite(dividend_I_2))
					dividend_I = dividend_I_1;
				else
					dividend_I = dividend_I_1 - dividend_I_2;

				double newValueI = dividend_R / divisor;
				double newValueQ = dividend_I / divisor;
				
				out[index - lowBound] = new Signal(newValueI, newValueQ);
			}
		} else
		{
			int mid = lowBound + (highBound - lowBound) / 2;
			ChannelRayleigh left  = new ChannelRayleigh(SNR, signals, lowBound, mid, sequentialThreshold);
			ChannelRayleigh right = new ChannelRayleigh(SNR, signals, mid, highBound, sequentialThreshold);
			left.fork();
			Signal[] rightAns = right.compute();
			Signal[] leftAns  = left.join();
			System.arraycopy(leftAns, 0, out, 0, leftAns.length);
			System.arraycopy(rightAns, 0, out, leftAns.length, rightAns.length);
			rightAns = null;
			leftAns = null;
		}
		return out;
	}

	public Signal[] noise(double _SNR, Signal[] _array) {
		return WorkersTools.fjPool.invoke(new ChannelRayleigh(_SNR, _array, 0, _array.length, WorkersTools.getSequentialThreshold(_array.length)));
	}
}
