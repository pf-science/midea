/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class StatisticsTools
{
    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

	public static double getBER(BinaryNumber[] _reference, BinaryNumber[] _distorted)
	{
		int blockLength = _reference[0].getLength();
		int sequenceLength = _reference.length;
		int correctBits = 0;
		for (int i = 0; i < sequenceLength; i++)
			correctBits += blockLength - _reference[i].lsum2(_distorted[i]).getWeight();
		double BER = 1.0 - (double)correctBits / (blockLength * (double)sequenceLength);
		return BER;
	}

	public static double getSER(BinaryNumber[] _reference, BinaryNumber[] _distorted)
	{
		int blockLength = _reference[0].getLength();
		int baskets = (int)Math.pow(2, blockLength);
		int[] counters = new int[baskets];
		int sequenceLength = _reference.length;
		for (int i = 0; i < baskets; i++)
			counters[i] = 0;
		for (BinaryNumber cbn: _distorted)
			counters[(int)cbn.toLong()]++;
		int correctSymbols = 0;
		for (int i = 0; i < sequenceLength; i++)
			if (_reference[i].toLong() == _distorted[i].toLong())
				correctSymbols++;
		double SER = 1.0 - (double)correctSymbols / (double)sequenceLength;
		return SER;
	}
	
	public static double decibelsToTimes(double _dB)
	{
		return Math.pow(10, _dB / 10);
	}
}
