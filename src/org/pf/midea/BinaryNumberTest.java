/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import org.junit.Assert;
import org.junit.Test;

public class BinaryNumberTest
{
    @Test
    public void test001_getLength()
    {
        BinaryNumber number = new BinaryNumber("1011011");
        Assert.assertEquals(7, number.getLength());
    }

    @Test
    public void test002_getValue()
    {
        BinaryNumber number = new BinaryNumber("1011011");
        Assert.assertEquals(91, number.toLong());
    }

    @Test
    public void test003_getStringSequence()
    {
        BinaryNumber number = new BinaryNumber("1011011");
        Assert.assertEquals("1011011", number.getStringSequence());
    }

    @Test
    public void test004_lsum2()
    {
        BinaryNumber number1 = new BinaryNumber("1011011");
        BinaryNumber number2 = new BinaryNumber("111");
        BinaryNumber number3 = number1.lsum2(number2);
        Assert.assertEquals("0101011", number3.getStringSequence());
    }

    @Test
    public void test005_rsum2()
    {
        BinaryNumber number1 = new BinaryNumber("1011011");
        BinaryNumber number2 = new BinaryNumber("111");
        BinaryNumber number3 = number1.rsum2(number2);
        Assert.assertEquals("1011100", number3.getStringSequence());
    }

    @Test
    public void test006_shl()
    {
        BinaryNumber number1 = new BinaryNumber("1011011");
        BinaryNumber number2 = number1.shl(2);
        Assert.assertEquals("101101100", number2.getStringSequence());
    }

    @Test
    public void test007_getWeight()
    {
        BinaryNumber number = new BinaryNumber("1011011");
        Assert.assertEquals(5, number.getWeight());
    }

    @Test
    public void test008_leaveLeft()
    {
        BinaryNumber number = new BinaryNumber("1011010");
        BinaryNumber result = number.leaveLeft(4);
        Assert.assertEquals("1011", result.getStringSequence());
    }

    @Test
    public void test009_leaveLeft()
    {
        BinaryNumber number = new BinaryNumber("1011010");
        BinaryNumber result = number.leaveLeft(10);
        Assert.assertEquals("1011010000", result.getStringSequence());
    }

    @Test
    public void test010_leaveRight()
    {
        BinaryNumber number = new BinaryNumber("1011010");
        BinaryNumber result = number.leaveRight(4);
        Assert.assertEquals("1010", result.getStringSequence());
    }

    @Test
    public void test011_leaveRight()
    {
        BinaryNumber number = new BinaryNumber("1011010");
        BinaryNumber result = number.leaveRight(10);
        Assert.assertEquals("0001011010", result.getStringSequence());
    }

    @Test
    public void test012_trimLeft()
    {
        BinaryNumber number = new BinaryNumber("0001010");
        BinaryNumber result = number.trimLeft();
        Assert.assertEquals("1010", result.getStringSequence());
    }
}