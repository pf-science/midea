/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class Constellations
{
	public static final ConstellationPoint[] mapGray8PSK = {
			new ConstellationPoint(new BinaryNumber("000"), 5 * Math.PI / 4),
			new ConstellationPoint(new BinaryNumber("001"), 4 * Math.PI / 4),
			new ConstellationPoint(new BinaryNumber("010"), 2 * Math.PI / 4),
			new ConstellationPoint(new BinaryNumber("011"), 3 * Math.PI / 4),
			new ConstellationPoint(new BinaryNumber("100"), 6 * Math.PI / 4),
			new ConstellationPoint(new BinaryNumber("101"), 7 * Math.PI / 4),
			new ConstellationPoint(new BinaryNumber("110"), 1 * Math.PI / 4),
			new ConstellationPoint(new BinaryNumber("111"), 0 * Math.PI / 4)
	};

	public static final ConstellationPoint[] mapGray16QAM = {
			new ConstellationPoint(new BinaryNumber("0000"), -3, -3),
			new ConstellationPoint(new BinaryNumber("0001"), -3, -1),
			new ConstellationPoint(new BinaryNumber("0010"), -3,  3),
			new ConstellationPoint(new BinaryNumber("0011"), -3,  1),
			new ConstellationPoint(new BinaryNumber("0100"), -1, -3),
			new ConstellationPoint(new BinaryNumber("0101"), -1, -1),
			new ConstellationPoint(new BinaryNumber("0110"), -1,  3),
			new ConstellationPoint(new BinaryNumber("0111"), -1,  1),
			new ConstellationPoint(new BinaryNumber("1000"),  3, -3),
			new ConstellationPoint(new BinaryNumber("1001"),  3, -1),
			new ConstellationPoint(new BinaryNumber("1010"),  3,  3),
			new ConstellationPoint(new BinaryNumber("1011"),  3,  1),
			new ConstellationPoint(new BinaryNumber("1100"),  1, -3),
			new ConstellationPoint(new BinaryNumber("1101"),  1, -1),
			new ConstellationPoint(new BinaryNumber("1110"),  1,  3),
			new ConstellationPoint(new BinaryNumber("1111"),  1,  1)
	};

	public static final ConstellationPoint[] mapGray16PSK = {
			new ConstellationPoint(new BinaryNumber("0000"),  0 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("0001"),  1 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("0010"),  3 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("0011"),  2 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("0100"),  7 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("0101"),  6 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("0110"),  4 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("0111"),  5 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1000"), 15 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1001"), 14 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1010"), 12 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1011"), 13 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1100"),  8 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1101"),  9 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1110"), 11 * Math.PI / 8),
			new ConstellationPoint(new BinaryNumber("1111"), 10 * Math.PI / 8)
	};

    public static final ConstellationPoint[] mapGray32PSK = {
            new ConstellationPoint(new BinaryNumber("00000"),  0 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("00001"),  1 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("00010"),  3 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("00011"),  2 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("00100"),  7 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("00101"),  6 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("00110"),  4 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("00111"),  5 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01000"),  15 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01001"),  14 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01010"),  12 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01011"),  13 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01100"),  8 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01101"),  9 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01110"),  11 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("01111"),  10 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10000"),  31 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10001"),  30 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10010"),  28 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10011"),  29 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10100"),  24 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10101"),  25 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10110"),  27 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("10111"),  26 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11000"),  16 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11001"),  17 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11010"),  19 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11011"),  18 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11100"),  23 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11101"),  22 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11110"),  20 * Math.PI / 16),
            new ConstellationPoint(new BinaryNumber("11111"),  21 * Math.PI / 16),
    };

    public static final ConstellationPoint[] mapGray32QAM = {
            new ConstellationPoint(new BinaryNumber("00000"),  -1,  -1),
            new ConstellationPoint(new BinaryNumber("00001"),  -1,  -3),
            new ConstellationPoint(new BinaryNumber("00010"),  -1,  1),
            new ConstellationPoint(new BinaryNumber("00011"),  -1,  3),
            new ConstellationPoint(new BinaryNumber("00100"),  -3,  -1),
            new ConstellationPoint(new BinaryNumber("00101"),  -3,  -3),
            new ConstellationPoint(new BinaryNumber("00110"),  -3,  1),
            new ConstellationPoint(new BinaryNumber("00111"),  -3,  3),
            new ConstellationPoint(new BinaryNumber("01000"),  1,  -1),
            new ConstellationPoint(new BinaryNumber("01001"),  1,  -3),
            new ConstellationPoint(new BinaryNumber("01010"),  1,  1),
            new ConstellationPoint(new BinaryNumber("01011"),  1,  3),
            new ConstellationPoint(new BinaryNumber("01100"),  3,  -1),
            new ConstellationPoint(new BinaryNumber("01101"),  3,  -3),
            new ConstellationPoint(new BinaryNumber("01110"),  3,  1),
            new ConstellationPoint(new BinaryNumber("01111"),  3,  3),
            new ConstellationPoint(new BinaryNumber("10000"),  -5, -3),
            new ConstellationPoint(new BinaryNumber("10001"),  -1, -5),
            new ConstellationPoint(new BinaryNumber("10010"),  -5, 3),
            new ConstellationPoint(new BinaryNumber("10011"),  -1, 5),
            new ConstellationPoint(new BinaryNumber("10100"),  -5, -1),
            new ConstellationPoint(new BinaryNumber("10101"),  -3, -5),
            new ConstellationPoint(new BinaryNumber("10110"),  -5, 1),
            new ConstellationPoint(new BinaryNumber("10111"),  -3, 5),
            new ConstellationPoint(new BinaryNumber("11000"),  5, -3),
            new ConstellationPoint(new BinaryNumber("11001"),  1, -5),
            new ConstellationPoint(new BinaryNumber("11010"),  5, 3),
            new ConstellationPoint(new BinaryNumber("11011"),  1, 5),
            new ConstellationPoint(new BinaryNumber("11100"),  5, -1),
            new ConstellationPoint(new BinaryNumber("11101"),  3, -5),
            new ConstellationPoint(new BinaryNumber("11110"),  5, 1),
            new ConstellationPoint(new BinaryNumber("11111"),  3, 5)
    };

	public static final ConstellationPoint[] mapGray64QAM = {
			new ConstellationPoint(new BinaryNumber("000000"),  7,  7),
			new ConstellationPoint(new BinaryNumber("000001"),  7,  5),
			new ConstellationPoint(new BinaryNumber("000010"),  5,  7),
			new ConstellationPoint(new BinaryNumber("000011"),  5,  5),
			new ConstellationPoint(new BinaryNumber("000100"),  7,  1),
			new ConstellationPoint(new BinaryNumber("000101"),  7,  3),
			new ConstellationPoint(new BinaryNumber("000110"),  5,  1),
			new ConstellationPoint(new BinaryNumber("000111"),  5,  3),
			new ConstellationPoint(new BinaryNumber("001000"),  1,  7),
			new ConstellationPoint(new BinaryNumber("001001"),  1,  5),
			new ConstellationPoint(new BinaryNumber("001010"),  3,  7),
			new ConstellationPoint(new BinaryNumber("001011"),  3,  5),
			new ConstellationPoint(new BinaryNumber("001100"),  1,  1),
			new ConstellationPoint(new BinaryNumber("001101"),  1,  3),
			new ConstellationPoint(new BinaryNumber("001110"),  3,  1),
			new ConstellationPoint(new BinaryNumber("001111"),  3,  3),
			new ConstellationPoint(new BinaryNumber("010000"),  7, -7),
			new ConstellationPoint(new BinaryNumber("010001"),  7, -5),
			new ConstellationPoint(new BinaryNumber("010010"),  5, -7),
			new ConstellationPoint(new BinaryNumber("010011"),  5, -5),
			new ConstellationPoint(new BinaryNumber("010100"),  7, -1),
			new ConstellationPoint(new BinaryNumber("010101"),  7, -3),
			new ConstellationPoint(new BinaryNumber("010110"),  5, -1),
			new ConstellationPoint(new BinaryNumber("010111"),  5, -3),
			new ConstellationPoint(new BinaryNumber("011000"),  1, -7),
			new ConstellationPoint(new BinaryNumber("011001"),  1, -5),
			new ConstellationPoint(new BinaryNumber("011010"),  3, -7),
			new ConstellationPoint(new BinaryNumber("011011"),  3, -5),
			new ConstellationPoint(new BinaryNumber("011100"),  1, -1),
			new ConstellationPoint(new BinaryNumber("011101"),  1, -3),
			new ConstellationPoint(new BinaryNumber("011110"),  3, -1),
			new ConstellationPoint(new BinaryNumber("011111"),  3, -3),
			new ConstellationPoint(new BinaryNumber("100000"), -7,  7),
			new ConstellationPoint(new BinaryNumber("100001"), -7,  5),
			new ConstellationPoint(new BinaryNumber("100010"), -5,  7),
			new ConstellationPoint(new BinaryNumber("100011"), -5,  5),
			new ConstellationPoint(new BinaryNumber("100100"), -7,  1),
			new ConstellationPoint(new BinaryNumber("100101"), -7,  3),
			new ConstellationPoint(new BinaryNumber("100110"), -5,  1),
			new ConstellationPoint(new BinaryNumber("100111"), -5,  3),
			new ConstellationPoint(new BinaryNumber("101000"), -1,  7),
			new ConstellationPoint(new BinaryNumber("101001"), -1,  5),
			new ConstellationPoint(new BinaryNumber("101010"), -3,  7),
			new ConstellationPoint(new BinaryNumber("101011"), -3,  5),
			new ConstellationPoint(new BinaryNumber("101100"), -1,  1),
			new ConstellationPoint(new BinaryNumber("101101"), -1,  3),
			new ConstellationPoint(new BinaryNumber("101110"), -3,  1),
			new ConstellationPoint(new BinaryNumber("101111"), -3,  3),
			new ConstellationPoint(new BinaryNumber("110000"), -7, -7),
			new ConstellationPoint(new BinaryNumber("110001"), -7, -5),
			new ConstellationPoint(new BinaryNumber("110010"), -5, -7),
			new ConstellationPoint(new BinaryNumber("110011"), -5, -5),
			new ConstellationPoint(new BinaryNumber("110100"), -7, -1),
			new ConstellationPoint(new BinaryNumber("110101"), -7, -3),
			new ConstellationPoint(new BinaryNumber("110110"), -5, -1),
			new ConstellationPoint(new BinaryNumber("110111"), -5, -3),
			new ConstellationPoint(new BinaryNumber("111000"), -1, -7),
			new ConstellationPoint(new BinaryNumber("111001"), -1, -5),
			new ConstellationPoint(new BinaryNumber("111010"), -3, -7),
			new ConstellationPoint(new BinaryNumber("111011"), -3, -5),
			new ConstellationPoint(new BinaryNumber("111100"), -1, -1),
			new ConstellationPoint(new BinaryNumber("111101"), -1, -3),
			new ConstellationPoint(new BinaryNumber("111110"), -3, -1),
			new ConstellationPoint(new BinaryNumber("111111"), -3, -3)
	};

    public static final ConstellationPoint[] mapGray256QAM = {
            new ConstellationPoint(new BinaryNumber("00000000"), -15, -15),
            new ConstellationPoint(new BinaryNumber("00000001"), -15, -13),
            new ConstellationPoint(new BinaryNumber("00000010"), -15, -9),
            new ConstellationPoint(new BinaryNumber("00000011"), -15, -11),
            new ConstellationPoint(new BinaryNumber("00000100"), -15, -1),
            new ConstellationPoint(new BinaryNumber("00000101"), -15, -3),
            new ConstellationPoint(new BinaryNumber("00000110"), -15, -7),
            new ConstellationPoint(new BinaryNumber("00000111"), -15, -5),
            new ConstellationPoint(new BinaryNumber("00001000"), -15,  15),
            new ConstellationPoint(new BinaryNumber("00001001"), -15,  13),
            new ConstellationPoint(new BinaryNumber("00001010"), -15,  9),
            new ConstellationPoint(new BinaryNumber("00001011"), -15,  11),
            new ConstellationPoint(new BinaryNumber("00001100"), -15,  1),
            new ConstellationPoint(new BinaryNumber("00001101"), -15,  3),
            new ConstellationPoint(new BinaryNumber("00001110"), -15,  7),
            new ConstellationPoint(new BinaryNumber("00001111"), -15,  5),
            new ConstellationPoint(new BinaryNumber("00010000"), -13, -15),
            new ConstellationPoint(new BinaryNumber("00010001"), -13, -13),
            new ConstellationPoint(new BinaryNumber("00010010"), -13, -9),
            new ConstellationPoint(new BinaryNumber("00010011"), -13, -11),
            new ConstellationPoint(new BinaryNumber("00010100"), -13, -1),
            new ConstellationPoint(new BinaryNumber("00010101"), -13, -3),
            new ConstellationPoint(new BinaryNumber("00010110"), -13, -7),
            new ConstellationPoint(new BinaryNumber("00010111"), -13, -5),
            new ConstellationPoint(new BinaryNumber("00011000"), -13,  15),
            new ConstellationPoint(new BinaryNumber("00011001"), -13,  13),
            new ConstellationPoint(new BinaryNumber("00011010"), -13,  9),
            new ConstellationPoint(new BinaryNumber("00011011"), -13,  11),
            new ConstellationPoint(new BinaryNumber("00011100"), -13,  1),
            new ConstellationPoint(new BinaryNumber("00011101"), -13,  3),
            new ConstellationPoint(new BinaryNumber("00011110"), -13,  7),
            new ConstellationPoint(new BinaryNumber("00011111"), -13,  5),
            new ConstellationPoint(new BinaryNumber("00100000"), -9,  -15),
            new ConstellationPoint(new BinaryNumber("00100001"), -9,  -13),
            new ConstellationPoint(new BinaryNumber("00100010"), -9,  -9),
            new ConstellationPoint(new BinaryNumber("00100011"), -9,  -11),
            new ConstellationPoint(new BinaryNumber("00100100"), -9,  -1),
            new ConstellationPoint(new BinaryNumber("00100101"), -9,  -3),
            new ConstellationPoint(new BinaryNumber("00100110"), -9,  -7),
            new ConstellationPoint(new BinaryNumber("00100111"), -9,  -5),
            new ConstellationPoint(new BinaryNumber("00101000"), -9,  15),
            new ConstellationPoint(new BinaryNumber("00101001"), -9,  13),
            new ConstellationPoint(new BinaryNumber("00101010"), -9,  9),
            new ConstellationPoint(new BinaryNumber("00101011"), -9,  11),
            new ConstellationPoint(new BinaryNumber("00101100"), -9,  1),
            new ConstellationPoint(new BinaryNumber("00101101"), -9,  3),
            new ConstellationPoint(new BinaryNumber("00101110"), -9,  7),
            new ConstellationPoint(new BinaryNumber("00101111"), -9,  5),
            new ConstellationPoint(new BinaryNumber("00110000"), -11, -15),
            new ConstellationPoint(new BinaryNumber("00110001"), -11, -13),
            new ConstellationPoint(new BinaryNumber("00110010"), -11, -9),
            new ConstellationPoint(new BinaryNumber("00110011"), -11, -11),
            new ConstellationPoint(new BinaryNumber("00110100"), -11, -1),
            new ConstellationPoint(new BinaryNumber("00110101"), -11, -3),
            new ConstellationPoint(new BinaryNumber("00110110"), -11, -7),
            new ConstellationPoint(new BinaryNumber("00110111"), -11, -5),
            new ConstellationPoint(new BinaryNumber("00111000"), -11, 15),
            new ConstellationPoint(new BinaryNumber("00111001"), -11, 13),
            new ConstellationPoint(new BinaryNumber("00111010"), -11, 9),
            new ConstellationPoint(new BinaryNumber("00111011"), -11, 11),
            new ConstellationPoint(new BinaryNumber("00111100"), -11, 1),
            new ConstellationPoint(new BinaryNumber("00111101"), -11, 3),
            new ConstellationPoint(new BinaryNumber("00111110"), -11, 7),
            new ConstellationPoint(new BinaryNumber("00111111"), -11, 5),
            new ConstellationPoint(new BinaryNumber("01000000"),  -1,  -15),
            new ConstellationPoint(new BinaryNumber("01000001"),  -1,  -13),
            new ConstellationPoint(new BinaryNumber("01000010"),  -1,  -9),
            new ConstellationPoint(new BinaryNumber("01000011"),  -1,  -11),
            new ConstellationPoint(new BinaryNumber("01000100"),  -1,  -1),
            new ConstellationPoint(new BinaryNumber("01000101"),  -1,  -3),
            new ConstellationPoint(new BinaryNumber("01000110"),  -1,  -7),
            new ConstellationPoint(new BinaryNumber("01000111"),  -1,  -5),
            new ConstellationPoint(new BinaryNumber("01001000"),  -1,  15),
            new ConstellationPoint(new BinaryNumber("01001001"),  -1,  13),
            new ConstellationPoint(new BinaryNumber("01001010"),  -1,  9),
            new ConstellationPoint(new BinaryNumber("01001011"),  -1,  11),
            new ConstellationPoint(new BinaryNumber("01001100"),  -1,  1),
            new ConstellationPoint(new BinaryNumber("01001101"),  -1,  3),
            new ConstellationPoint(new BinaryNumber("01001110"),  -1,  7),
            new ConstellationPoint(new BinaryNumber("01001111"),  -1,  5),
            new ConstellationPoint(new BinaryNumber("01010000"),  -3, -15),
            new ConstellationPoint(new BinaryNumber("01010001"),  -3, -13),
            new ConstellationPoint(new BinaryNumber("01010010"),  -3, -9),
            new ConstellationPoint(new BinaryNumber("01010011"),  -3, -11),
            new ConstellationPoint(new BinaryNumber("01010100"),  -3, -1),
            new ConstellationPoint(new BinaryNumber("01010101"),  -3, -3),
            new ConstellationPoint(new BinaryNumber("01010110"),  -3, -7),
            new ConstellationPoint(new BinaryNumber("01010111"),  -3, -5),
            new ConstellationPoint(new BinaryNumber("01011000"),  -3, 15),
            new ConstellationPoint(new BinaryNumber("01011001"),  -3, 13),
            new ConstellationPoint(new BinaryNumber("01011010"),  -3, 9),
            new ConstellationPoint(new BinaryNumber("01011011"),  -3, 11),
            new ConstellationPoint(new BinaryNumber("01011100"),  -3, 1),
            new ConstellationPoint(new BinaryNumber("01011101"),  -3, 3),
            new ConstellationPoint(new BinaryNumber("01011110"),  -3, 7),
            new ConstellationPoint(new BinaryNumber("01011111"),  -3, 5),
            new ConstellationPoint(new BinaryNumber("01100000"), -7,  -15),
            new ConstellationPoint(new BinaryNumber("01100001"), -7,  -13),
            new ConstellationPoint(new BinaryNumber("01100010"), -7,  -9),
            new ConstellationPoint(new BinaryNumber("01100011"), -7,  -11),
            new ConstellationPoint(new BinaryNumber("01100100"), -7,  -1),
            new ConstellationPoint(new BinaryNumber("01100101"), -7,  -3),
            new ConstellationPoint(new BinaryNumber("01100110"), -7,  -7),
            new ConstellationPoint(new BinaryNumber("01100111"), -7,  -5),
            new ConstellationPoint(new BinaryNumber("01101000"), -7,  15),
            new ConstellationPoint(new BinaryNumber("01101001"), -7,  13),
            new ConstellationPoint(new BinaryNumber("01101010"), -7,  9),
            new ConstellationPoint(new BinaryNumber("01101011"), -7,  11),
            new ConstellationPoint(new BinaryNumber("01101100"), -7,  1),
            new ConstellationPoint(new BinaryNumber("01101101"), -7,  3),
            new ConstellationPoint(new BinaryNumber("01101110"), -7,  7),
            new ConstellationPoint(new BinaryNumber("01101111"), -7,  5),
            new ConstellationPoint(new BinaryNumber("01110000"), -5, -15 ),
            new ConstellationPoint(new BinaryNumber("01110001"), -5, -13),
            new ConstellationPoint(new BinaryNumber("01110010"), -5, -9),
            new ConstellationPoint(new BinaryNumber("01110011"), -5, -11),
            new ConstellationPoint(new BinaryNumber("01110100"), -5, -1),
            new ConstellationPoint(new BinaryNumber("01110101"), -5, -3),
            new ConstellationPoint(new BinaryNumber("01110110"), -5, -7),
            new ConstellationPoint(new BinaryNumber("01110111"), -5, -5),
            new ConstellationPoint(new BinaryNumber("01111000"), -5, 15),
            new ConstellationPoint(new BinaryNumber("01111001"), -5, 13),
            new ConstellationPoint(new BinaryNumber("01111010"), -5, 9),
            new ConstellationPoint(new BinaryNumber("01111011"), -5, 11),
            new ConstellationPoint(new BinaryNumber("01111100"), -5, 1),
            new ConstellationPoint(new BinaryNumber("01111101"), -5, 3),
            new ConstellationPoint(new BinaryNumber("01111110"), -5, 7),
            new ConstellationPoint(new BinaryNumber("01111111"), -5, 5),
            new ConstellationPoint(new BinaryNumber("10000000"),  15,  -15),
            new ConstellationPoint(new BinaryNumber("10000001"),  15,  -13),
            new ConstellationPoint(new BinaryNumber("10000010"),  15,  -9),
            new ConstellationPoint(new BinaryNumber("10000011"),  15,  -11),
            new ConstellationPoint(new BinaryNumber("10000100"),  15,  -1),
            new ConstellationPoint(new BinaryNumber("10000101"),  15,  -3),
            new ConstellationPoint(new BinaryNumber("10000110"),  15,  -7),
            new ConstellationPoint(new BinaryNumber("10000111"),  15,  -5),
            new ConstellationPoint(new BinaryNumber("10001000"),  15,  15),
            new ConstellationPoint(new BinaryNumber("10001001"),  15,  13),
            new ConstellationPoint(new BinaryNumber("10001010"),  15,  9),
            new ConstellationPoint(new BinaryNumber("10001011"),  15,  11),
            new ConstellationPoint(new BinaryNumber("10001100"),  15,  1),
            new ConstellationPoint(new BinaryNumber("10001101"),  15,  3),
            new ConstellationPoint(new BinaryNumber("10001110"),  15,  7),
            new ConstellationPoint(new BinaryNumber("10001111"),  15,  5),
            new ConstellationPoint(new BinaryNumber("10010000"),  13, -15),
            new ConstellationPoint(new BinaryNumber("10010001"),  13, -13),
            new ConstellationPoint(new BinaryNumber("10010010"),  13, -9),
            new ConstellationPoint(new BinaryNumber("10010011"),  13, -11),
            new ConstellationPoint(new BinaryNumber("10010100"),  13, -1),
            new ConstellationPoint(new BinaryNumber("10010101"),  13, -3),
            new ConstellationPoint(new BinaryNumber("10010110"),  13, -7),
            new ConstellationPoint(new BinaryNumber("10010111"),  13, -5),
            new ConstellationPoint(new BinaryNumber("10011000"),  13, 15),
            new ConstellationPoint(new BinaryNumber("10011001"),  13, 13),
            new ConstellationPoint(new BinaryNumber("10011010"),  13, 9),
            new ConstellationPoint(new BinaryNumber("10011011"),  13, 11),
            new ConstellationPoint(new BinaryNumber("10011100"),  13, 1),
            new ConstellationPoint(new BinaryNumber("10011101"),  13, 3),
            new ConstellationPoint(new BinaryNumber("10011110"),  13, 7),
            new ConstellationPoint(new BinaryNumber("10011111"),  13, 5),
            new ConstellationPoint(new BinaryNumber("10100000"), 9,  -15),
            new ConstellationPoint(new BinaryNumber("10100001"), 9,  -13),
            new ConstellationPoint(new BinaryNumber("10100010"), 9,  -9),
            new ConstellationPoint(new BinaryNumber("10100011"), 9,  -11),
            new ConstellationPoint(new BinaryNumber("10100100"), 9,  -1),
            new ConstellationPoint(new BinaryNumber("10100101"), 9,  -3),
            new ConstellationPoint(new BinaryNumber("10100110"), 9,  -7),
            new ConstellationPoint(new BinaryNumber("10100111"), 9,  -5),
            new ConstellationPoint(new BinaryNumber("10101000"), 9,  15),
            new ConstellationPoint(new BinaryNumber("10101001"), 9,  13),
            new ConstellationPoint(new BinaryNumber("10101010"), 9,  9),
            new ConstellationPoint(new BinaryNumber("10101011"), 9,  11),
            new ConstellationPoint(new BinaryNumber("10101100"), 9,  1),
            new ConstellationPoint(new BinaryNumber("10101101"), 9,  3),
            new ConstellationPoint(new BinaryNumber("10101110"), 9,  7),
            new ConstellationPoint(new BinaryNumber("10101111"), 9,  5),
            new ConstellationPoint(new BinaryNumber("10110000"), 11, -15),
            new ConstellationPoint(new BinaryNumber("10110001"), 11, -13),
            new ConstellationPoint(new BinaryNumber("10110010"), 11, -9),
            new ConstellationPoint(new BinaryNumber("10110011"), 11, -11),
            new ConstellationPoint(new BinaryNumber("10110100"), 11, -1),
            new ConstellationPoint(new BinaryNumber("10110101"), 11, -3),
            new ConstellationPoint(new BinaryNumber("10110110"), 11, -7),
            new ConstellationPoint(new BinaryNumber("10110111"), 11, -5),
            new ConstellationPoint(new BinaryNumber("10111000"), 11, 15),
            new ConstellationPoint(new BinaryNumber("10111001"), 11, 13),
            new ConstellationPoint(new BinaryNumber("10111010"), 11, 9),
            new ConstellationPoint(new BinaryNumber("10111011"), 11, 11),
            new ConstellationPoint(new BinaryNumber("10111100"), 11, 1),
            new ConstellationPoint(new BinaryNumber("10111101"), 11, 3),
            new ConstellationPoint(new BinaryNumber("10111110"), 11, 7),
            new ConstellationPoint(new BinaryNumber("10111111"), 11, 5),
            new ConstellationPoint(new BinaryNumber("11000000"),  1,  -15),
            new ConstellationPoint(new BinaryNumber("11000001"),  1,  -13),
            new ConstellationPoint(new BinaryNumber("11000010"),  1,  -9),
            new ConstellationPoint(new BinaryNumber("11000011"),  1,  -11),
            new ConstellationPoint(new BinaryNumber("11000100"),  1,  -1),
            new ConstellationPoint(new BinaryNumber("11000101"),  1,  -3),
            new ConstellationPoint(new BinaryNumber("11000110"),  1,  -7),
            new ConstellationPoint(new BinaryNumber("11000111"),  1,  -5),
            new ConstellationPoint(new BinaryNumber("11001000"),  1,  15),
            new ConstellationPoint(new BinaryNumber("11001001"),  1,  13),
            new ConstellationPoint(new BinaryNumber("11001010"),  1,  9),
            new ConstellationPoint(new BinaryNumber("11001011"),  1,  11),
            new ConstellationPoint(new BinaryNumber("11001100"),  1,  1),
            new ConstellationPoint(new BinaryNumber("11001101"),  1,  3),
            new ConstellationPoint(new BinaryNumber("11001110"),  1,  7),
            new ConstellationPoint(new BinaryNumber("11001111"),  1,  5),
            new ConstellationPoint(new BinaryNumber("11010000"),  3, -15),
            new ConstellationPoint(new BinaryNumber("11010001"),  3, -13),
            new ConstellationPoint(new BinaryNumber("11010010"),  3, -9),
            new ConstellationPoint(new BinaryNumber("11010011"),  3, -11),
            new ConstellationPoint(new BinaryNumber("11010100"),  3, -1),
            new ConstellationPoint(new BinaryNumber("11010101"),  3, -3),
            new ConstellationPoint(new BinaryNumber("11010110"),  3, -7),
            new ConstellationPoint(new BinaryNumber("11010111"),  3, -5),
            new ConstellationPoint(new BinaryNumber("11011000"),  3, 15),
            new ConstellationPoint(new BinaryNumber("11011001"),  3, 13),
            new ConstellationPoint(new BinaryNumber("11011010"),  3, 9),
            new ConstellationPoint(new BinaryNumber("11011011"),  3, 11),
            new ConstellationPoint(new BinaryNumber("11011100"),  3, 1),
            new ConstellationPoint(new BinaryNumber("11011101"),  3, 3),
            new ConstellationPoint(new BinaryNumber("11011110"),  3, 7),
            new ConstellationPoint(new BinaryNumber("11011111"),  3, 5),
            new ConstellationPoint(new BinaryNumber("11100000"), 7,  -15),
            new ConstellationPoint(new BinaryNumber("11100001"), 7,  -13),
            new ConstellationPoint(new BinaryNumber("11100010"), 7,  -9),
            new ConstellationPoint(new BinaryNumber("11100011"), 7,  -11),
            new ConstellationPoint(new BinaryNumber("11100100"), 7,  -1),
            new ConstellationPoint(new BinaryNumber("11100101"), 7,  -3),
            new ConstellationPoint(new BinaryNumber("11100110"), 7,  -7),
            new ConstellationPoint(new BinaryNumber("11100111"), 7,  -5),
            new ConstellationPoint(new BinaryNumber("11101000"), 7,  15),
            new ConstellationPoint(new BinaryNumber("11101001"), 7,  13),
            new ConstellationPoint(new BinaryNumber("11101010"), 7,  9),
            new ConstellationPoint(new BinaryNumber("11101011"), 7,  11),
            new ConstellationPoint(new BinaryNumber("11101100"), 7,  1),
            new ConstellationPoint(new BinaryNumber("11101101"), 7,  3),
            new ConstellationPoint(new BinaryNumber("11101110"), 7,  7),
            new ConstellationPoint(new BinaryNumber("11101111"), 7,  5),
            new ConstellationPoint(new BinaryNumber("11110000"), 5, -15),
            new ConstellationPoint(new BinaryNumber("11110001"), 5, -13),
            new ConstellationPoint(new BinaryNumber("11110010"), 5, -9),
            new ConstellationPoint(new BinaryNumber("11110011"), 5, -11),
            new ConstellationPoint(new BinaryNumber("11110100"), 5, -1),
            new ConstellationPoint(new BinaryNumber("11110101"), 5, -3),
            new ConstellationPoint(new BinaryNumber("11110110"), 5, -7),
            new ConstellationPoint(new BinaryNumber("11110111"), 5, -5),
            new ConstellationPoint(new BinaryNumber("11111000"), 5, 15),
            new ConstellationPoint(new BinaryNumber("11111001"), 5, 13),
            new ConstellationPoint(new BinaryNumber("11111010"), 5, 9),
            new ConstellationPoint(new BinaryNumber("11111011"), 5, 11),
            new ConstellationPoint(new BinaryNumber("11111100"), 5, 1),
            new ConstellationPoint(new BinaryNumber("11111101"), 5, 3),
            new ConstellationPoint(new BinaryNumber("11111110"), 5, 7),
            new ConstellationPoint(new BinaryNumber("11111111"), 5, 5)
    };

	public static final ConstellationPoint[] mapASK = {
			new ConstellationPoint(new BinaryNumber("0"), 0, 0),
			new ConstellationPoint(new BinaryNumber("1"), 1, 0)
	};

	public static final ConstellationPoint[] mapBPSK = {
			new ConstellationPoint(new BinaryNumber("0"), 1 * Math.PI),
			new ConstellationPoint(new BinaryNumber("1"), 0 * Math.PI)
	};

	public static final ConstellationPoint[] mapFSK = {
			new ConstellationPoint(new BinaryNumber("0"), 1 * Math.PI / 2),
			new ConstellationPoint(new BinaryNumber("1"), 0 * Math.PI / 2)
	};

	public static final ConstellationPoint[] mapGrayQPSK = {
			new ConstellationPoint(new BinaryNumber("00"), Math.PI / 4 + 2 * Math.PI / 2),
			new ConstellationPoint(new BinaryNumber("01"), Math.PI / 4 + 1 * Math.PI / 2),
			new ConstellationPoint(new BinaryNumber("10"), Math.PI / 4 + 3 * Math.PI / 2),
			new ConstellationPoint(new BinaryNumber("11"), Math.PI / 4 + 0 * Math.PI / 2)
	};
}
