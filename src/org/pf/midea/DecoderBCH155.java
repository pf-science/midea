/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.RecursiveTask;

public class DecoderBCH155 extends RecursiveTask<BinaryNumber[]> implements Decoder
{
    private long extraBitsDecoded = 0;
    private BinaryNumber[] sequence;
    private int lowBound, highBound, sequentialThreshold;

    public DecoderBCH155()
    {

    }

    public DecoderBCH155(BinaryNumber[] _sequence, int _lowBound, int _highBound, int _sequentialThreshold)
    {
        sequence = _sequence;
        lowBound = _lowBound;
        highBound = _highBound;
        sequentialThreshold = _sequentialThreshold;
    }

    @Override
    public BinaryNumber[] compute()
    {
        BinaryNumber[] out = new BinaryNumber[highBound - lowBound];
        if (highBound - lowBound <= sequentialThreshold)
        {
            for (int index = lowBound; index < highBound; ++index)
            {
                BinaryNumber syndrome = sequence[index].divmod2(new BinaryNumber("10100110111")).leaveRight(10);
                BinaryNumber errorsVector = DecoderSyndromes.syndromesMapBCH155.getErrorVector(syndrome);
                BinaryNumber corrected;
                if (errorsVector != null)
                    corrected = sequence[index].lsum2(errorsVector);
                else
                    corrected = sequence[index];
                out[index - lowBound] = corrected.leaveLeft(5);
            }
        } else
        {
            int mid = lowBound + (highBound - lowBound) / 2;
            DecoderBCH155 left  = new DecoderBCH155(sequence, lowBound, mid, sequentialThreshold);
            DecoderBCH155 right = new DecoderBCH155(sequence, mid, highBound, sequentialThreshold);
            left.fork();
            BinaryNumber[] rightAns = right.compute();
            BinaryNumber[] leftAns  = left.join();
            System.arraycopy(leftAns, 0, out, 0, leftAns.length);
            System.arraycopy(rightAns, 0, out, leftAns.length, rightAns.length);
            rightAns = null;
            leftAns = null;
        }
        return out;
    }

    public long getExtraBitsDecoded()
    {
        return extraBitsDecoded;
    }

    public BinaryNumber[] decode(BinaryNumber[] _sequence)
    {
        long originalBlockSize = _sequence[0].getLength();
        Shaper shaper = new Shaper(_sequence, 15);
        BinaryNumber[] shaped = shaper.reshape();
        extraBitsDecoded += shaper.getExtraBits();
        BinaryNumber[] preshaped = WorkersTools.fjPool.invoke(new DecoderBCH155(shaped, 0, shaped.length, WorkersTools.getSequentialThreshold(shaped.length)));
        shaper = new Shaper(preshaped, originalBlockSize);
        BinaryNumber[] ret = shaper.reshape();
        extraBitsDecoded += shaper.getExtraBits();
        return ret;
    }
}
