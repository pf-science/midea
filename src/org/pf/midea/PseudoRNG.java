/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class PseudoRNG
{
	private static long seed = System.currentTimeMillis();
	private static final double mod = Math.pow(2, 63);
	
	public static double nextDouble()
	{
		seed ^= (seed << 21);
		seed ^= (seed >>> 35);
		seed ^= (seed << 4);
		return Math.abs((seed % mod) / mod);
	}

	public static double nextGaussian()
    {
		double r, x, y;
		do
		{
			x = 2.0 * nextDouble() - 1.0;
			y = 2.0 * nextDouble() - 1.0;
			r = x*x + y*y;
		} while (r > 1 || r == 0);
		return x * Math.sqrt(-2.0 * Math.log(r) / r);
    }

    public static long nextLong(long _bound)
    {
        return Math.round(nextDouble() * _bound);
    }
}
