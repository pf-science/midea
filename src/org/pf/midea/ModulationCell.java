/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class ModulationCell
{
    private PlanStates.ModulationType modulationType;

    public ModulationCell(PlanStates.ModulationType _modulationType)
    {
        modulationType = _modulationType;
    }

    public PlanStates.ModulationType getModulationType()
    {
        return modulationType;
    }

    public String getModulationTypeName()
    {
        String out = null;
        switch (modulationType)
        {
            case MT_ASK:
                out = "АМн";
                break;
            case MT_FSK:
                out = "ЧМн";
                break;
            case MT_BPSK:
                out = "ФМн";
                break;
            case MT_QPSK:
                out = "ФМ-4";
                break;
            case MT_8PSK:
                out = "ФМ-8";
                break;
            case MT_16PSK:
                out = "ФМ-16";
                break;
            case MT_32PSK:
                out = "ФМ-32";
                break;
            case MT_16QAM:
                out = "КАМ-16";
                break;
            case MT_32QAM:
                out = "КАМ-32";
                break;
            case MT_64QAM:
                out = "КАМ-64";
                break;
            case MT_256QAM:
                out = "КАМ-256";
                break;
        }
        return out;
    }
}
