/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Map;

public class HumanNumberFormat extends NumberFormat
{
	private int precision;
	private Map<Character, Character> superscripts = new HashMap<Character, Character>() {{
		put('-', '⁻');
		put('0', '⁰');
		put('1', '¹');
		put('2', '²');
		put('3', '³');
		put('4', '⁴');
		put('5', '⁵');
		put('6', '⁶');
		put('7', '⁷');
		put('8', '⁸');
		put('9', '⁹');
		put(',', '⋅');
		put('.', '⋅');
	}};

	public HumanNumberFormat(int _precision)
	{
		precision = _precision;
	}

	private String internalFormat(double _value)
	{
		//gets absolute value of given number
		double absoluteValue = Math.abs(_value);
		//calculates power of number
		double power = absoluteValue == 0 ? 0 : Math.log10(absoluteValue);
		//replaces power digits with superscript Unicode characters
		String powerString = String.format("%1." + precision + "f", power);
		String powerSuperString = "";
		for (int i = 0; i < powerString.length(); i++)
			powerSuperString += superscripts.get(powerString.charAt(i));
		//formes resulting string including number sign
		String numberSignum = _value < 0 ? "-" : "";
		String numberCharacteristic = power == 0 ? "" : powerSuperString;
		String numberMantissa = power == 0 ? "1" : "10";
		return numberSignum + numberMantissa + numberCharacteristic;
	}

	@Override
	public StringBuffer format(double v, StringBuffer stringBuffer, FieldPosition fieldPosition)
	{
		return stringBuffer.append(internalFormat(v));
	}

	@Override
	public StringBuffer format(long l, StringBuffer stringBuffer, FieldPosition fieldPosition)
	{
		return stringBuffer.append(internalFormat(l));
	}

	@Override
	public Number parse(String s, ParsePosition parsePosition)
	{
		return null;
	}
}
