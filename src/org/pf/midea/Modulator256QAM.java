/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.RecursiveTask;

public class Modulator256QAM extends RecursiveTask<Signal[]> implements Modulator
{
    public static final double normalizationFactor = 1 / (15 * Math.sqrt(2));

    private BinaryNumber[] bits;
    private int lowBound, highBound, sequentialThreshold;

    public Modulator256QAM()
    {

    }

    public Modulator256QAM(BinaryNumber[] _bits, int _lowBound, int _highBound, int _sequentialThreshold)
    {
        bits = _bits;
        lowBound = _lowBound;
        highBound = _highBound;
        sequentialThreshold = _sequentialThreshold;
    }

    @Override
    protected Signal[] compute()
    {
        Signal[] out = new Signal[highBound - lowBound];
        if (highBound - lowBound <= sequentialThreshold)
        {
            for (int index = lowBound; index < highBound; ++index)
            {
                double valueI = 0, valueQ = 0;
                for (ConstellationPoint ccp: Constellations.mapGray256QAM)
                    if (ccp.getCode().toLong() == bits[index].toLong())
                    {
                        valueI = ccp.getI() * normalizationFactor;
                        valueQ = ccp.getQ() * normalizationFactor;
                        break;
                    }
                out[index - lowBound] = new Signal(valueI, valueQ);
            }
        } else
        {
            int mid = lowBound + (highBound - lowBound) / 2;
            Modulator256QAM left  = new Modulator256QAM(bits, lowBound, mid, sequentialThreshold);
            Modulator256QAM right = new Modulator256QAM(bits, mid, highBound, sequentialThreshold);
            left.fork();
            Signal[] rightAns = right.compute();
            Signal[] leftAns  = left.join();
            System.arraycopy(leftAns, 0, out, 0, leftAns.length);
            System.arraycopy(rightAns, 0, out, leftAns.length, rightAns.length);
            rightAns = null;
            leftAns = null;
        }
        return out;
    }

    public Signal[] modulate(BinaryNumber[] _array)
    {
        return WorkersTools.fjPool.invoke(new Modulator256QAM(_array, 0, _array.length, WorkersTools.getSequentialThreshold(_array.length)));
    }
}
