/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.awt.*;

public class ColorCell
{
    private Color color;

    public ColorCell(Color _color)
    {
        color = _color;
    }

    public Color getColor()
    {
        return color;
    }

    public String getColorName()
    {
        String out = null;
        if (color == null)
            out = "Автоматично";
        else if (color == Color.black || color == Color.BLACK)
            out = "Чорний";
        else if (color == Color.red || color == Color.RED)
            out = "Червоний";
        if (color == Color.green || color == Color.GREEN)
            out = "Зелений";
        if (color == Color.blue || color == Color.blue)
            out = "Синій";
        return out;
    }
}
