/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.awt.*;

public class PlanCell
{
    private SourceCell sourceCell;
    private CodeCell codeCell;
    private ModulationCell modulationCell;
    private ChannelCell channelCell;
    private ErrorsCell errorsCell;
    private int lineWidth;
    private ColorCell colorCell;

    public PlanCell(SourceCell _sourceCell, CodeCell _codeCell, ModulationCell _modulationCell, ChannelCell _channelCell, ErrorsCell _errorsCell, int _lineWidth, ColorCell _colorCell)
    {
        sourceCell = _sourceCell;
        codeCell = _codeCell;
        modulationCell = _modulationCell;
        channelCell = _channelCell;
        errorsCell = _errorsCell;
        lineWidth = _lineWidth;
        colorCell = _colorCell;
    }

    public String getDescription()
    {
        return getSourceTypeName() + ";" + getCodeTypeName() + ";" + getModulationTypeName() + ";" + getChannelTypeName() + ";" + getErrorsTypeName() + ";" + getLineWidthDescription() + ";" + getLineColorDescription();
    }

    public String getShortDescription()
    {
        return getChannelTypeName() + ", " + getModulationTypeName() + ", " + getCodeTypeName() +  ", " + getErrorsTypeName();
    }

    public SourceCell getSourceCell()
    {
        return sourceCell;
    }

    public CodeCell getCodeCell()
    {
        return codeCell;
    }

    public ModulationCell getModulationCell()
    {
        return modulationCell;
    }

    public ChannelCell getChannelCell()
    {
        return channelCell;
    }

    public ErrorsCell getErrorsCell()
    {
        return errorsCell;
    }

    public int getLineWidth()
    {
        return lineWidth;
    }

    public Color getLineColor()
    {
        return colorCell.getColor();
    }

    public String getSourceTypeName()
    {
        return sourceCell.getSourceTypeName();
    }

    public String getCodeTypeName()
    {
        return codeCell.getCodeTypeName();
    }

    public String getModulationTypeName()
    {
        return modulationCell.getModulationTypeName();
    }

    public String getChannelTypeName()
    {
        return channelCell.getChannelTypeName();
    }

    public String getErrorsTypeName()
    {
        return errorsCell.getErrorsTypeName();
    }

    public String getLineWidthDescription()
    {
        return String.valueOf(lineWidth) + "пікс";
    }

    public String getLineColorDescription()
    {
        return colorCell.getColorName();
    }
}
