/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.ArrayList;
import java.util.Arrays;

public class ConstellationPointsGenerator
{
    public static BinaryNumber[] getTestPoints(PlanStates.ModulationType _modulationType, int _repeats)
    {
        ArrayList<BinaryNumber> out = new ArrayList<>();
        switch (_modulationType)
        {
            case MT_ASK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsASK.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPointsASK));
                break;
            case MT_FSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsFSK.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPointsFSK));
                break;
            case MT_BPSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsBPSK.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPointsBPSK));
                break;
            case MT_QPSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsQPSK.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPointsQPSK));
                break;
            case MT_8PSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints8PSK.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPoints8PSK));
                break;
            case MT_16PSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints16PSK.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPoints16PSK));
                break;
            case MT_32PSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints32PSK.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPoints32PSK));
                break;
            case MT_16QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints16QAM.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPoints16QAM));
                break;
            case MT_32QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints32QAM.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPoints32QAM));
                break;
            case MT_64QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints64QAM.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPoints64QAM));
                break;
            case MT_256QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints256QAM.length; i++)
                    out.addAll(Arrays.asList(TestConstellationPoints.testPoints256QAM));
                break;
        }
        return out.toArray(new BinaryNumber[out.size()]);
    }

    public static BinaryNumber[] getRandomPoints(PlanStates.ModulationType _modulationType, int _repeats)
    {
        ArrayList<BinaryNumber> out = new ArrayList<>();
        switch (_modulationType)
        {
            case MT_ASK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsASK.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPointsASK)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(2 - 1), 1));
                break;
            case MT_FSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsFSK.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPointsFSK)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(2 - 1), 1));
                break;
            case MT_BPSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsBPSK.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPointsBPSK)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(2 - 1), 1));
                break;
            case MT_QPSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPointsQPSK.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPointsQPSK)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(4 - 1), 2));
                break;
            case MT_8PSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints8PSK.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPoints8PSK)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(8 - 1), 3));
                break;
            case MT_16PSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints16PSK.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPoints16PSK)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(16 - 1), 4));
                break;
            case MT_32PSK:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints32PSK.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPoints32PSK)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(32 - 1), 5));
                break;
            case MT_16QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints16QAM.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPoints16QAM)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(16 - 1), 4));
                break;
            case MT_32QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints32QAM.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPoints32QAM)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(32 - 1), 5));
                break;
            case MT_64QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints64QAM.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPoints64QAM)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(64 - 1), 6));
                break;
            case MT_256QAM:
                for (int i = 0; i < _repeats / TestConstellationPoints.testPoints256QAM.length; i++)
                    for (BinaryNumber ignored: TestConstellationPoints.testPoints256QAM)
                        out.add(new BinaryNumber(PseudoRNG.nextLong(256 - 1), 8));
                break;
        }
        return out.toArray(new BinaryNumber[out.size()]);
    }
}
