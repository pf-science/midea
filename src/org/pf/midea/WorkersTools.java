/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.ForkJoinPool;

public class WorkersTools
{
	public static ForkJoinPool fjPool = new ForkJoinPool();
	
	public static int getSequentialThreshold(int _arrayLength)
	{
		int threshold = _arrayLength / Runtime.getRuntime().availableProcessors();
		if (threshold < 1)
			threshold = 1;
		return threshold;
	}
}
