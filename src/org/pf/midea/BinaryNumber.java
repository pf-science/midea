/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class BinaryNumber
{
    private boolean[] data;
	private long value = 0;

	private long getValue()
	{
		long out = 0;
		for (int i = 0; i < getLength(); i++)
			if (data[i])
				out += Math.pow(2, getLength() - i - 1);
		return out;
	}

    public BinaryNumber(String _sequence)
    {
		int size = _sequence.length();
		data = new boolean[size];
		for (int i = 0; i < size; i++)
			data[i] = _sequence.charAt(i) == '1';
		value = getValue();
    }
	
	public BinaryNumber(boolean[] _sequence)
	{
		data = _sequence;
		value = getValue();
	}

    public BinaryNumber(long _value, long _alignment)
    {
        String sequence = Long.toBinaryString(_value);
        String leadingZeroes = "";
        for (int i = 0; i < _alignment - sequence.length(); i++)
            leadingZeroes += "0";
        sequence = leadingZeroes + sequence;
        int size = sequence.length();
        data = new boolean[size];
        for (int i = 0; i < size; i++)
            data[i] = sequence.charAt(i) == '1';
        value = getValue();
    }

    public String getStringSequence()
    {
		String out = "";
		for (int i = 0; i < data.length; i++)
			out += getDigit(i) ? "1" : "0";
		return out;
    }

    public boolean[] getData()
    {
        return data;
    }

    public long toLong()
    {
		return value;
    }

    public BinaryNumber lsum2(BinaryNumber _number)
    {
		int maxLength = Math.max(getLength(), _number.getLength());
		int minLength = Math.min(getLength(), _number.getLength());
		boolean[] out = new boolean[maxLength];
		for (int i = 0; i < minLength; i++)
			out[i] = getDigit(i) ^ _number.getDigit(i);
		if (getLength() >= _number.getLength())
		{
			for (int i = minLength; i < maxLength; i++)
				out[i] = getDigit(i);
		} else
		{
			for (int i = minLength; i < maxLength; i++)
				out[i] = _number.getDigit(i);
		}
		return new BinaryNumber(out);
    }

    public BinaryNumber rsum2(BinaryNumber _number)
    {
        int maxLength = Math.max(getLength(), _number.getLength());
        int minLength = Math.min(getLength(), _number.getLength());
        boolean[] out;
        if (getLength() >= _number.getLength())
        {
            out = getData();
            for (int i = maxLength - minLength; i < maxLength; i++)
                out[i] = getDigit(i) ^ _number.getDigit(i - maxLength + minLength);
        } else
        {
            out = _number.getData();
            for (int i = maxLength - minLength; i < maxLength; i++)
                out[i] = _number.getDigit(i) ^ getDigit(i - maxLength + minLength);
        }
        return new BinaryNumber(out);
    }

    public BinaryNumber shl(long _value)
    {
        return new BinaryNumber(value << _value, getLength() + _value);
    }

    public BinaryNumber divmod2(BinaryNumber _number)
    {
        BinaryNumber dividend = new BinaryNumber(data);
        do {
            dividend = dividend.lsum2(_number).trimLeft();
        } while (dividend.getLength() >= _number.getLength() && dividend.getValue() != 0);
        return dividend;
    }

    public BinaryNumber trimLeft()
    {
        int count = 0;
        int index = 0;
        while (!getDigit(index++) && index < getLength())
            count++;
        return leaveRight(getLength() - count);
    }

    public BinaryNumber leaveLeft(int _value)
    {
        boolean[] newData = new boolean[_value];
        if (getLength() < _value)
        {
            System.arraycopy(getData(), 0, newData, 0, getLength());
        } else
            System.arraycopy(getData(), 0, newData, 0, _value);
        return new BinaryNumber(newData);
    }

    public BinaryNumber leaveRight(int _value)
    {
        boolean[] newData = new boolean[_value];
        if (getLength() < _value)
        {
            System.arraycopy(getData(), 0, newData, _value - getLength(), getLength());
        } else
            System.arraycopy(getData(), getLength() - _value, newData, 0, _value);
        return new BinaryNumber(newData);
    }
	
	public boolean getDigit(int _index)
	{
		return data[_index];
	}

    public int getWeight()
    {
		int out = 0;
		for (int i = 0; i < getLength(); i++)
			if (getDigit(i))
				out++;
		return out;
    }

    public int getLength()
    {
		return data.length;
    }
}
