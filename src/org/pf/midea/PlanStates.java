/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class PlanStates
{
    enum SourceType
    {
        ST_TEST,
        ST_RANDOM
    }

    enum CodeType
    {
        CT_NONE,
        CT_HAMMING74,
        CT_CYCLIC,
        CT_BCH155
    }

    enum ModulationType
    {
        MT_ASK,
        MT_FSK,
        MT_BPSK,
        MT_QPSK,
        MT_8PSK,
        MT_16PSK,
        MT_32PSK,
        MT_16QAM,
        MT_32QAM,
        MT_64QAM,
        MT_256QAM
    }

    enum ChannelType
    {
        CHT_AWGN,
        CHT_RAYLEIGH
    }

    enum ErrorsType
    {
        ET_SER,
        ET_BER
    }
}
