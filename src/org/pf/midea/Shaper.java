/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

public class Shaper
{
    private BinaryNumber[] sequence;
    private long blockLength, extraBits = 0;

    public Shaper(BinaryNumber[] _sequence, long _blockLength)
    {
        sequence = _sequence;
        blockLength = _blockLength;
    }

    public BinaryNumber[] reshape()
    {
        int commonSize = 0;
        for (BinaryNumber cbn: sequence)
            commonSize += cbn.getLength();

        if (commonSize % (int)blockLength != 0)
            extraBits = ((int)blockLength - commonSize % (int)blockLength);
        int newSize = commonSize + (int)extraBits;

        boolean[] linear = new boolean[newSize];

        int index = 0;
        for (BinaryNumber cbn: sequence)
        {
            System.arraycopy(cbn.getData(), 0, linear, index, cbn.getLength());
            index += cbn.getLength();
        }

        BinaryNumber[] out = new BinaryNumber[newSize / (int)blockLength];
        index = 0;
        for (int i = 0; i < out.length; i++)
        {
            boolean[] newBlock = new boolean[(int)blockLength];
            System.arraycopy(linear, index, newBlock, 0, (int)blockLength);
            out[i] = new BinaryNumber(newBlock);
            index += blockLength;
        }

        return out;
    }

    public long getExtraBits()
    {
        return extraBits;
    }
}
