/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.RecursiveTask;

public class Demodulator256QAM extends RecursiveTask<BinaryNumber[]> implements Demodulator
{
    public static final double BOUND2 = 2 / (15 * Math.sqrt(2));
    public static final double BOUND4 = 4 / (15 * Math.sqrt(2));
    public static final double BOUND6 = 6 / (15 * Math.sqrt(2));
    public static final double BOUND8 = 8 / (15 * Math.sqrt(2));
    public static final double BOUND10 = 10 / (15 * Math.sqrt(2));
    public static final double BOUND12 = 12 / (15 * Math.sqrt(2));
    public static final double BOUND14 = 14 / (15 * Math.sqrt(2));

    private Signal[] signals;
    private int lowBound, highBound, sequentialThreshold;

    public Demodulator256QAM()
    {

    }

    public Demodulator256QAM(Signal[] _signals, int _lowBound, int _highBound, int _sequentialThreshold)
    {
        signals = _signals;
        lowBound = _lowBound;
        highBound = _highBound;
        sequentialThreshold = _sequentialThreshold;
    }

    @Override
    public BinaryNumber[] compute()
    {
        BinaryNumber[] out = new BinaryNumber[highBound - lowBound];
        if (highBound - lowBound <= sequentialThreshold)
        {
            for (int index = lowBound; index < highBound; ++index)
            {
                double realI = signals[index].getI(), realQ = signals[index].getQ();
                double foundI = 0, foundQ = 0;
                if (realI <= -BOUND14)
                    foundI = -15;
                else if (realI >= -BOUND14 && realI <= -BOUND12)
                    foundI = -13;
                else if (realI >= -BOUND12 && realI <= -BOUND10)
                    foundI = -11;
                else if (realI >= -BOUND10 && realI <= -BOUND8)
                    foundI = -9;
                else if (realI >= -BOUND8 && realI <= -BOUND6)
                    foundI = -7;
                else if (realI >= -BOUND6 && realI <= -BOUND4)
                    foundI = -5;
                else if (realI >= -BOUND4 && realI <= -BOUND2)
                    foundI = -3;
                else if (realI >= -BOUND2 && realI <= 0)
                    foundI = -1;
                else if (realI >= 0 && realI <= BOUND2)
                    foundI = 1;
                else if (realI >= BOUND2 && realI <= BOUND4)
                    foundI = 3;
                else if (realI >= BOUND4 && realI <= BOUND6)
                    foundI = 5;
                else if (realI >= BOUND6 && realI <= BOUND8)
                    foundI = 7;
                else if (realI >= BOUND8 && realI <= BOUND10)
                    foundI = 9;
                else if (realI >= BOUND10 && realI <= BOUND12)
                    foundI = 11;
                else if (realI >= BOUND12 && realI <= BOUND14)
                    foundI = 13;
                else if (realI >= BOUND14)
                    foundI = 15;

                if (realQ <= -BOUND14)
                    foundQ = -15;
                else if (realQ >= -BOUND14 && realQ <= -BOUND12)
                    foundQ = -13;
                else if (realQ >= -BOUND12 && realQ <= -BOUND10)
                    foundQ = -11;
                else if (realQ >= -BOUND10 && realQ <= -BOUND8)
                    foundQ = -9;
                else if (realQ >= -BOUND8 && realQ <= -BOUND6)
                    foundQ = -7;
                else if (realQ >= -BOUND6 && realQ <= -BOUND4)
                    foundQ = -5;
                else if (realQ >= -BOUND4 && realQ <= -BOUND2)
                    foundQ = -3;
                else if (realQ >= -BOUND2 && realQ <= 0)
                    foundQ = -1;
                else if (realQ >= 0 && realQ <= BOUND2)
                    foundQ = 1;
                else if (realQ >= BOUND2 && realQ <= BOUND4)
                    foundQ = 3;
                else if (realQ >= BOUND4 && realQ <= BOUND6)
                    foundQ = 5;
                else if (realQ >= BOUND6 && realQ <= BOUND8)
                    foundQ = 7;
                else if (realQ >= BOUND8 && realQ <= BOUND10)
                    foundQ = 9;
                else if (realQ >= BOUND10 && realQ <= BOUND12)
                    foundQ = 11;
                else if (realQ >= BOUND12 && realQ <= BOUND14)
                    foundQ = 13;
                else if (realQ >= BOUND14)
                    foundQ = 15;

                BinaryNumber code = null;
                for (ConstellationPoint ccp: Constellations.mapGray256QAM)
                    if (ccp.getI() == foundI && ccp.getQ() == foundQ)
                    {
                        code = ccp.getCode();
                        break;
                    }
                out[index - lowBound] = code;
            }
        } else
        {
            int mid = lowBound + (highBound - lowBound) / 2;
            Demodulator256QAM left  = new Demodulator256QAM(signals, lowBound, mid, sequentialThreshold);
            Demodulator256QAM right = new Demodulator256QAM(signals, mid, highBound, sequentialThreshold);
            left.fork();
            BinaryNumber[] rightAns = right.compute();
            BinaryNumber[] leftAns  = left.join();
            System.arraycopy(leftAns, 0, out, 0, leftAns.length);
            System.arraycopy(rightAns, 0, out, leftAns.length, rightAns.length);
            rightAns = null;
            leftAns = null;
        }
        return out;
    }

    public BinaryNumber[] demodulate(Signal[] _array)
    {
        return WorkersTools.fjPool.invoke(new Demodulator256QAM(_array, 0, _array.length, WorkersTools.getSequentialThreshold(_array.length)));
    }
}
