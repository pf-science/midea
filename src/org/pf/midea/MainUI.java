/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.CustomXYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainUI extends JPanel {
    public MainUI() {
        initComponents();
    }

    ArrayList<PlanCell> planCells = new ArrayList<>();

    private void updatePlanList()
    {
        scrollPane1.removeAll();
        panelPlan.removeAll();
        listPlan = null;
        scrollPane1 = null;
        listPlan = new JList(planCells.toArray(new PlanCell[planCells.size()]));
        listPlan.setCellRenderer(new PlanCellRenderer());
        scrollPane1 = new JScrollPane(listPlan);
        panelPlan.add(scrollPane1, new CellConstraints());
        panelPlan.updateUI();
    }

    private void showConstellationWindow(ConstellationPoint[] _map, String _name)
    {
        JFrame constellation = new JFrame("Сигнальне сузір’я " + _name);
        constellation.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        XYSeriesCollection dots = new XYSeriesCollection();
        XYSeries series = new XYSeries(_name);
        JFreeChart chart = ChartFactory.createScatterPlot("", "I", "Q", dots, PlotOrientation.VERTICAL, false, false, false);
        XYPlot xyPlot = chart.getXYPlot();
        CustomXYToolTipGenerator tooltipsGenerator = new CustomXYToolTipGenerator();
        ArrayList<String> tooltips = new ArrayList<>();
        for (ConstellationPoint ccp: _map)
        {
            double I = ccp.getI();
            double Q = ccp.getQ();
            series.add(I, Q);
            tooltips.add(ccp.getCode().getStringSequence());
        }
        tooltipsGenerator.addToolTipSeries(tooltips);
        xyPlot.getRenderer().setBaseToolTipGenerator(tooltipsGenerator);
        double maxX = StatisticsTools.round(Math.abs(series.getMaxX()), 3);
        double maxY = StatisticsTools.round(Math.abs(series.getMaxY()), 3);
        double minX = StatisticsTools.round(Math.abs(series.getMinX()), 3);
        double minY = StatisticsTools.round(Math.abs(series.getMinY()), 3);
        if (maxX != 0 || minX != 0)
        {
            double X = Math.max(minX, maxX);
            xyPlot.getDomainAxis().setRange(-1.1 * X, 1.1 * X);
        }
        else
            xyPlot.getDomainAxis().setRange(-1, 1);
        if (maxY != 0 || minY != 0)
        {
            double Y = Math.max(minY, maxY);
            xyPlot.getRangeAxis().setRange(-1.1 * Y, 1.1 * Y);
        }
        else
            xyPlot.getRangeAxis().setRange(-1, 1);
        dots.addSeries(series);

        xyPlot.setBackgroundPaint(Color.WHITE);
        xyPlot.setDomainGridlinePaint(Color.GRAY);
        xyPlot.setRangeGridlinePaint(Color.GRAY);
        xyPlot.getRenderer().setSeriesPaint(0, Color.BLACK);
        xyPlot.setDomainZeroBaselineVisible(true);
        xyPlot.setRangeZeroBaselineVisible(true);

        ChartPanel chartPanel = new ChartPanel(chart);
        JPanel nestedPanel = new JPanel();
        nestedPanel.add(chartPanel, new CellConstraints());
        constellation.add(nestedPanel);
        constellation.pack();
        constellation.setLocationRelativeTo(null);
        constellation.setResizable(false);
        constellation.setVisible(true);
    }

    private void buttonAddToPlanActionPerformed(ActionEvent e) {
        planCells.add(new PlanCell((SourceCell)chooserSource.getSelectedItem(), (CodeCell)chooserCode.getSelectedItem(), (ModulationCell)chooserModulation.getSelectedItem(), (ChannelCell)chooserChannel.getSelectedItem(), (ErrorsCell)chooserErrors.getSelectedItem(), (Integer)spinnerLineWidth.getValue(), (ColorCell)chooserLineColor.getSelectedItem()));
        updatePlanList();
    }

    private void createUIComponents() {
    }

    private void buttonRunSimulationActionPerformed(ActionEvent e) {
        if (planCells.size() != 0)
        {
            boolean dBs = radioButtonMeasurementUnitdB.isSelected();
            SimulationController simulationController = new SimulationController(planCells, spinnerHSquareLow, spinnerHSquareHigh, spinnerHSquareStep, spinnerIterations, dBs, progressBar, panelChart, textAreaNumeric, checkBoxShowLineNumbers.isSelected());
            Thread thread = new Thread(simulationController);
            thread.start();
        }
    }

    private void buttonRemoveFromPlanActionPerformed(ActionEvent e) {
        planCells.remove(listPlan.getSelectedValue());
        updatePlanList();
    }

    private void buttonClearPlanActionPerformed(ActionEvent e) {
        planCells.clear();
        updatePlanList();
    }

    private void menuItemQuitActionPerformed(ActionEvent e) {
        System.exit(0);
    }

    private void menuItemASKActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapASK, "АМн");
    }

    private void menuItemFSKActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapFSK, "ЧМн");
    }

    private void menuItemBPSKActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapBPSK, "ФМн");
    }

    private void menuItemQPSKActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGrayQPSK, "ФМ-4");
    }

    private void menuItem8PSKActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGray8PSK, "ФМ-8");
    }

    private void menuItem16PSKActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGray16PSK, "ФМ-16");
    }

    private void menuItem32PSKActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGray32PSK, "ФМ-32");
    }

    private void menuItem16QAMActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGray16QAM, "КАМ-16");
    }

    private void menuItem32QAMActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGray32QAM, "КАМ-32");
    }

    private void menuItem64QAMActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGray64QAM, "КАМ-64");
    }

    private void menuItemAboutActionPerformed(ActionEvent e) {
        JFrame about = new JFrame("Про програму");
        about.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        JPanel nestedPanel = new JPanel();
        nestedPanel.setLayout(new GridLayout(17, 1));
        nestedPanel.add(new JLabel("Дослідницький експериментальний комплекс"));
        nestedPanel.add(new JLabel("для математичного моделювання"));
        nestedPanel.add(new JLabel("завадозахищених каналів електрозв’язку"));
        nestedPanel.add(new JLabel(""));
        nestedPanel.add(new JLabel("© О. І. Наталенко, 2007–2013"));
        nestedPanel.add(new JLabel("Створено для ІТС НТУУ «КПІ»"));
        nestedPanel.add(new JLabel(""));
        nestedPanel.add(new JLabel("Цей програмний продукт розповсюджується"));
        nestedPanel.add(new JLabel("згідно з умовами Універсальної програмної Ліцензії версії 4.2."));
        nestedPanel.add(new JLabel("Додаткову інформацію про ліцензування можна дізнатися"));
        nestedPanel.add(new JLabel("з файлу COPYING (постачається із програмою)."));
        nestedPanel.add(new JLabel(""));
        nestedPanel.add(new JLabel("Зауваження й побажання приймаються за адресою:"));
        JLabel labelMail = new JLabel("<html><a href=\"mailto:oleksandr@natalenko.name\">oleksandr@natalenko.name</a></html>");
        labelMail.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().mail(new URI("mailto:oleksandr@natalenko.name?subject=" + URLEncoder.encode("Дослідницький експериментальний комплекс", "UTF-8")));
                } catch (URISyntaxException | IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        nestedPanel.add(labelMail);
        nestedPanel.add(new JLabel(""));
        nestedPanel.add(new JLabel("Веб-сторінка розробника:"));
        JLabel labelURL = new JLabel("<html><a href=\"http://natalenko.name/\">http://natalenko.name/</a></html>");
        labelURL.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(new URI("http://natalenko.name/"));
                } catch (URISyntaxException | IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        nestedPanel.add(labelURL);
        about.add(nestedPanel);
        about.pack();
        about.setLocationRelativeTo(null);
        about.setVisible(true);
    }

    private void menuItem256QAMActionPerformed(ActionEvent e) {
        showConstellationWindow(Constellations.mapGray256QAM, "КАМ-256");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        menuMain = new JMenuBar();
        menuFile = new JMenu();
        menuItemQuit = new JMenuItem();
        menuTools = new JMenu();
        menuConstellations = new JMenu();
        menuItemASK = new JMenuItem();
        menuItemFSK = new JMenuItem();
        menuItemBPSK = new JMenuItem();
        menuItemQPSK = new JMenuItem();
        menuItem8PSK = new JMenuItem();
        menuItem16PSK = new JMenuItem();
        menuItem32PSK = new JMenuItem();
        menuItem16QAM = new JMenuItem();
        menuItem32QAM = new JMenuItem();
        menuItem64QAM = new JMenuItem();
        menuItem256QAM = new JMenuItem();
        menuHelp = new JMenu();
        menuItemAbout = new JMenuItem();
        labelSource = new JLabel();
        SourceCell[] sourceCells = {
        new SourceCell(PlanStates.SourceType.ST_TEST),
        new SourceCell(PlanStates.SourceType.ST_RANDOM)
        };
        chooserSource = new JComboBox(sourceCells);
        chooserSource.setRenderer(new SourceCellRenderer());
        buttonAddToPlan = new JButton();
        panelPlan = new JPanel();
        scrollPane1 = new JScrollPane();
        listPlan = new JList();
        labelCode = new JLabel();
        CodeCell[] codeCells = {
        new CodeCell(PlanStates.CodeType.CT_NONE),
        new CodeCell(PlanStates.CodeType.CT_HAMMING74),
        new CodeCell(PlanStates.CodeType.CT_CYCLIC),
        new CodeCell(PlanStates.CodeType.CT_BCH155)
        };
        chooserCode = new JComboBox(codeCells);
        chooserCode.setRenderer(new CodeCellRenderer());
        labelModulation = new JLabel();
        ModulationCell[] modulationCells = {
        new ModulationCell(PlanStates.ModulationType.MT_ASK),
        new ModulationCell(PlanStates.ModulationType.MT_FSK),
        new ModulationCell(PlanStates.ModulationType.MT_BPSK),
        new ModulationCell(PlanStates.ModulationType.MT_QPSK),
        new ModulationCell(PlanStates.ModulationType.MT_8PSK),
        new ModulationCell(PlanStates.ModulationType.MT_16PSK),
        new ModulationCell(PlanStates.ModulationType.MT_32PSK),
        new ModulationCell(PlanStates.ModulationType.MT_16QAM),
        new ModulationCell(PlanStates.ModulationType.MT_32QAM),
        new ModulationCell(PlanStates.ModulationType.MT_64QAM),
        new ModulationCell(PlanStates.ModulationType.MT_256QAM)
        };
        chooserModulation = new JComboBox(modulationCells);
        chooserModulation.setRenderer(new ModulationCellRenderer());
        labelChannel = new JLabel();
        ChannelCell[] channelCells = {
        new ChannelCell(PlanStates.ChannelType.CHT_AWGN),
        new ChannelCell(PlanStates.ChannelType.CHT_RAYLEIGH)
        };
        chooserChannel = new JComboBox(channelCells);
        chooserChannel.setRenderer(new ChannelCellRenderer());
        buttonClearPlan = new JButton();
        labelErrors = new JLabel();
        ErrorsCell[] errorCells = {
        new ErrorsCell(PlanStates.ErrorsType.ET_SER),
        new ErrorsCell(PlanStates.ErrorsType.ET_BER)
        };
        chooserErrors = new JComboBox(errorCells);
        chooserErrors.setRenderer(new ErrorsCellRenderer());
        labelLineWidth = new JLabel();
        spinnerLineWidth = new JSpinner();
        buttonRemoveFromPlan = new JButton();
        labelLineColor = new JLabel();
        ColorCell[] colorCells = {
        new ColorCell(null),
        new ColorCell(Color.black),
        new ColorCell(Color.red),
        new ColorCell(Color.green),
        new ColorCell(Color.blue)
        };
        chooserLineColor = new JComboBox(colorCells);
        chooserLineColor.setRenderer(new ColorCellRenderer());
        labelHSquare = new JLabel();
        spinnerHSquareLow = new JSpinner();
        labelEllipsis = new JLabel();
        spinnerHSquareHigh = new JSpinner();
        panelInner1 = new JPanel();
        labelHSquareStep = new JLabel();
        spinnerHSquareStep = new JSpinner();
        labelMeasurementUnit = new JLabel();
        radioButtonMeasurementUnitTimes = new JRadioButton();
        radioButtonMeasurementUnitdB = new JRadioButton();
        buttonRunSimulation = new JButton();
        labelIterations = new JLabel();
        spinnerIterations = new JSpinner();
        checkBoxShowLineNumbers = new JCheckBox();
        tabbedPaneResults = new JTabbedPane();
        panelChart = new JPanel();
        panelNumeric = new JPanel();
        scrollPane2 = new JScrollPane();
        textAreaNumeric = new JTextArea();
        progressBar = new JProgressBar();

        //======== this ========
        setLayout(new FormLayout(
            "3*(default, $lcgap), 36dlu, $lcgap, 122dlu:grow",
            "[21px,default], 9*($lgap, default), $lgap, default:grow, $lgap, default"));

        //======== menuMain ========
        {

            //======== menuFile ========
            {
                menuFile.setText("\u0424\u0430\u0439\u043b");

                //---- menuItemQuit ----
                menuItemQuit.setText("\u0412\u0438\u0439\u0442\u0438");
                menuItemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
                menuItemQuit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItemQuitActionPerformed(e);
                    }
                });
                menuFile.add(menuItemQuit);
            }
            menuMain.add(menuFile);

            //======== menuTools ========
            {
                menuTools.setText("\u0406\u043d\u0441\u0442\u0440\u0443\u043c\u0435\u043d\u0442\u0438");

                //======== menuConstellations ========
                {
                    menuConstellations.setText("\u0421\u0438\u0433\u043d\u0430\u043b\u044c\u043d\u0456 \u0441\u0443\u0437\u0456\u0440\u2019\u044f");

                    //---- menuItemASK ----
                    menuItemASK.setText("\u0410\u041c\u043d\u2026");
                    menuItemASK.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItemASKActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItemASK);

                    //---- menuItemFSK ----
                    menuItemFSK.setText("\u0427\u041c\u043d\u2026");
                    menuItemFSK.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItemFSKActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItemFSK);

                    //---- menuItemBPSK ----
                    menuItemBPSK.setText("\u0424\u041c\u043d\u2026");
                    menuItemBPSK.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItemBPSKActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItemBPSK);

                    //---- menuItemQPSK ----
                    menuItemQPSK.setText("\u0424\u041c-4\u2026");
                    menuItemQPSK.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItemQPSKActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItemQPSK);

                    //---- menuItem8PSK ----
                    menuItem8PSK.setText("\u0424\u041c-8\u2026");
                    menuItem8PSK.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItem8PSKActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItem8PSK);

                    //---- menuItem16PSK ----
                    menuItem16PSK.setText("\u0424\u041c-16\u2026");
                    menuItem16PSK.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItem16PSKActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItem16PSK);

                    //---- menuItem32PSK ----
                    menuItem32PSK.setText("\u0424\u041c-32\u2026");
                    menuItem32PSK.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItem32PSKActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItem32PSK);

                    //---- menuItem16QAM ----
                    menuItem16QAM.setText("\u041a\u0410\u041c-16\u2026");
                    menuItem16QAM.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItem16QAMActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItem16QAM);

                    //---- menuItem32QAM ----
                    menuItem32QAM.setText("\u041a\u0410\u041c-32\u2026");
                    menuItem32QAM.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItem32QAMActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItem32QAM);

                    //---- menuItem64QAM ----
                    menuItem64QAM.setText("\u041a\u0410\u041c-64\u2026");
                    menuItem64QAM.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItem64QAMActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItem64QAM);

                    //---- menuItem256QAM ----
                    menuItem256QAM.setText("\u041a\u0410\u041c-256\u2026");
                    menuItem256QAM.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            menuItem256QAMActionPerformed(e);
                        }
                    });
                    menuConstellations.add(menuItem256QAM);
                }
                menuTools.add(menuConstellations);
            }
            menuMain.add(menuTools);

            //======== menuHelp ========
            {
                menuHelp.setText("\u0414\u043e\u0432\u0456\u0434\u043a\u0430");

                //---- menuItemAbout ----
                menuItemAbout.setText("\u041f\u0440\u043e \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u0443\u2026");
                menuItemAbout.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItemAboutActionPerformed(e);
                    }
                });
                menuHelp.add(menuItemAbout);
            }
            menuMain.add(menuHelp);
        }
        add(menuMain, CC.xywh(1, 1, 9, 1, CC.FILL, CC.FILL));

        //---- labelSource ----
        labelSource.setText("\u0414\u0436\u0435\u0440\u0435\u043b\u043e:");
        add(labelSource, CC.xy(1, 3));
        add(chooserSource, CC.xy(3, 3));

        //---- buttonAddToPlan ----
        buttonAddToPlan.setText("\u2192");
        buttonAddToPlan.setToolTipText("\u0414\u043e\u0434\u0430\u0442\u0438 \u0434\u043e \u043f\u043b\u0430\u043d\u0443 \u0435\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0443");
        buttonAddToPlan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAddToPlanActionPerformed(e);
            }
        });
        add(buttonAddToPlan, CC.xywh(5, 3, 1, 5));

        //======== panelPlan ========
        {
            panelPlan.setLayout(new GridLayout());

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(listPlan);
            }
            panelPlan.add(scrollPane1);
        }
        add(panelPlan, CC.xywh(7, 3, 3, 13));

        //---- labelCode ----
        labelCode.setText("\u041a\u043e\u0434:");
        add(labelCode, CC.xy(1, 5));
        add(chooserCode, CC.xy(3, 5));

        //---- labelModulation ----
        labelModulation.setText("\u041c\u043e\u0434\u0443\u043b\u044f\u0446\u0456\u044f:");
        add(labelModulation, CC.xy(1, 7));
        add(chooserModulation, CC.xy(3, 7));

        //---- labelChannel ----
        labelChannel.setText("\u041a\u0430\u043d\u0430\u043b:");
        add(labelChannel, CC.xy(1, 9));
        add(chooserChannel, CC.xy(3, 9));

        //---- buttonClearPlan ----
        buttonClearPlan.setText("X");
        buttonClearPlan.setToolTipText("\u041e\u0447\u0438\u0441\u0442\u0438\u0442\u0438 \u043f\u043b\u0430\u043d \u0435\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0443");
        buttonClearPlan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonClearPlanActionPerformed(e);
            }
        });
        add(buttonClearPlan, CC.xy(5, 9));

        //---- labelErrors ----
        labelErrors.setText("\u041f\u043e\u043c\u0438\u043b\u043a\u0438:");
        add(labelErrors, CC.xy(1, 11));
        add(chooserErrors, CC.xy(3, 11));

        //---- labelLineWidth ----
        labelLineWidth.setText("\u0422\u043e\u0432\u0449\u0438\u043d\u0430 \u043b\u0456\u043d\u0456\u0457:");
        add(labelLineWidth, CC.xy(1, 13));

        //---- spinnerLineWidth ----
        spinnerLineWidth.setModel(new SpinnerNumberModel(2, 1, 5, 1));
        add(spinnerLineWidth, CC.xy(3, 13));

        //---- buttonRemoveFromPlan ----
        buttonRemoveFromPlan.setText("\u2190");
        buttonRemoveFromPlan.setToolTipText("\u0412\u0438\u0434\u0430\u043b\u0438\u0442\u0438 \u0437 \u043f\u043b\u0430\u043d\u0443 \u0435\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0443");
        buttonRemoveFromPlan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonRemoveFromPlanActionPerformed(e);
            }
        });
        add(buttonRemoveFromPlan, CC.xywh(5, 11, 1, 5));

        //---- labelLineColor ----
        labelLineColor.setText("\u041a\u043e\u043b\u0456\u0440 \u043b\u0456\u043d\u0456\u0457:");
        add(labelLineColor, CC.xy(1, 15));
        add(chooserLineColor, CC.xy(3, 15));

        //---- labelHSquare ----
        labelHSquare.setText("h\u00b2=");
        add(labelHSquare, CC.xy(1, 17));

        //---- spinnerHSquareLow ----
        spinnerHSquareLow.setModel(new SpinnerNumberModel(0.0, null, null, 1.0));
        add(spinnerHSquareLow, CC.xy(3, 17));

        //---- labelEllipsis ----
        labelEllipsis.setText("\u2026");
        add(labelEllipsis, CC.xy(5, 17, CC.CENTER, CC.DEFAULT));

        //---- spinnerHSquareHigh ----
        spinnerHSquareHigh.setModel(new SpinnerNumberModel(15.0, null, null, 1.0));
        add(spinnerHSquareHigh, CC.xy(7, 17));

        //======== panelInner1 ========
        {
            panelInner1.setLayout(new FormLayout(
                "5*(default, $lcgap), default:grow",
                "default"));

            //---- labelHSquareStep ----
            labelHSquareStep.setText(", \u043a\u0440\u043e\u043a:");
            panelInner1.add(labelHSquareStep, CC.xy(1, 1));

            //---- spinnerHSquareStep ----
            spinnerHSquareStep.setModel(new SpinnerNumberModel(0.5, 0.001, null, 0.1));
            panelInner1.add(spinnerHSquareStep, CC.xy(3, 1));

            //---- labelMeasurementUnit ----
            labelMeasurementUnit.setText(", \u043e\u0434\u0438\u043d\u0438\u0446\u0456:");
            panelInner1.add(labelMeasurementUnit, CC.xy(5, 1));

            //---- radioButtonMeasurementUnitTimes ----
            radioButtonMeasurementUnitTimes.setText("\u0440\u0430\u0437\u0438");
            radioButtonMeasurementUnitTimes.setSelected(true);
            panelInner1.add(radioButtonMeasurementUnitTimes, CC.xy(7, 1, CC.LEFT, CC.DEFAULT));

            //---- radioButtonMeasurementUnitdB ----
            radioButtonMeasurementUnitdB.setText("\u0434\u0411");
            panelInner1.add(radioButtonMeasurementUnitdB, CC.xy(9, 1, CC.LEFT, CC.DEFAULT));

            //---- buttonRunSimulation ----
            buttonRunSimulation.setText("\u0412\u0438\u043a\u043e\u043d\u0430\u0442\u0438");
            buttonRunSimulation.setToolTipText("\u0417\u0430\u043f\u0443\u0441\u0442\u0438\u0442\u0438 \u043c\u043e\u0434\u0435\u043b\u044e\u0432\u0430\u043d\u043d\u044f");
            buttonRunSimulation.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    buttonRunSimulationActionPerformed(e);
                }
            });
            panelInner1.add(buttonRunSimulation, CC.xy(11, 1, CC.RIGHT, CC.DEFAULT));
        }
        add(panelInner1, CC.xy(9, 17));

        //---- labelIterations ----
        labelIterations.setText("\u0406\u0442\u0435\u0440\u0430\u0446\u0456\u0439:");
        add(labelIterations, CC.xy(1, 19));

        //---- spinnerIterations ----
        spinnerIterations.setModel(new SpinnerNumberModel(200000, 1, null, 50000));
        add(spinnerIterations, CC.xy(3, 19));

        //---- checkBoxShowLineNumbers ----
        checkBoxShowLineNumbers.setText("\u041d\u0443\u043c\u0435\u0440\u0443\u0432\u0430\u0442\u0438 \u0433\u0440\u0430\u0444\u0456\u043a\u0438");
        add(checkBoxShowLineNumbers, CC.xywh(7, 19, 3, 1));

        //======== tabbedPaneResults ========
        {

            //======== panelChart ========
            {
                panelChart.setLayout(new BoxLayout(panelChart, BoxLayout.X_AXIS));
            }
            tabbedPaneResults.addTab("\u0413\u0440\u0430\u0444\u0456\u043a\u0438", panelChart);


            //======== panelNumeric ========
            {
                panelNumeric.setLayout(new FormLayout(
                    "default:grow",
                    "default:grow"));

                //======== scrollPane2 ========
                {

                    //---- textAreaNumeric ----
                    textAreaNumeric.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
                    textAreaNumeric.setEditable(false);
                    scrollPane2.setViewportView(textAreaNumeric);
                }
                panelNumeric.add(scrollPane2, CC.xy(1, 1, CC.DEFAULT, CC.FILL));
            }
            tabbedPaneResults.addTab("\u0427\u0438\u0441\u043b\u043e\u0432\u0456 \u0434\u0430\u043d\u0456", panelNumeric);

        }
        add(tabbedPaneResults, CC.xywh(1, 21, 9, 1, CC.DEFAULT, CC.FILL));
        add(progressBar, CC.xywh(1, 23, 9, 1));

        //---- buttonGroupMeasurementUnit ----
        ButtonGroup buttonGroupMeasurementUnit = new ButtonGroup();
        buttonGroupMeasurementUnit.add(radioButtonMeasurementUnitTimes);
        buttonGroupMeasurementUnit.add(radioButtonMeasurementUnitdB);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JMenuBar menuMain;
    private JMenu menuFile;
    private JMenuItem menuItemQuit;
    private JMenu menuTools;
    private JMenu menuConstellations;
    private JMenuItem menuItemASK;
    private JMenuItem menuItemFSK;
    private JMenuItem menuItemBPSK;
    private JMenuItem menuItemQPSK;
    private JMenuItem menuItem8PSK;
    private JMenuItem menuItem16PSK;
    private JMenuItem menuItem32PSK;
    private JMenuItem menuItem16QAM;
    private JMenuItem menuItem32QAM;
    private JMenuItem menuItem64QAM;
    private JMenuItem menuItem256QAM;
    private JMenu menuHelp;
    private JMenuItem menuItemAbout;
    private JLabel labelSource;
    private JComboBox chooserSource;
    private JButton buttonAddToPlan;
    private JPanel panelPlan;
    private JScrollPane scrollPane1;
    private JList listPlan;
    private JLabel labelCode;
    private JComboBox chooserCode;
    private JLabel labelModulation;
    private JComboBox chooserModulation;
    private JLabel labelChannel;
    private JComboBox chooserChannel;
    private JButton buttonClearPlan;
    private JLabel labelErrors;
    private JComboBox chooserErrors;
    private JLabel labelLineWidth;
    private JSpinner spinnerLineWidth;
    private JButton buttonRemoveFromPlan;
    private JLabel labelLineColor;
    private JComboBox chooserLineColor;
    private JLabel labelHSquare;
    private JSpinner spinnerHSquareLow;
    private JLabel labelEllipsis;
    private JSpinner spinnerHSquareHigh;
    private JPanel panelInner1;
    private JLabel labelHSquareStep;
    private JSpinner spinnerHSquareStep;
    private JLabel labelMeasurementUnit;
    private JRadioButton radioButtonMeasurementUnitTimes;
    private JRadioButton radioButtonMeasurementUnitdB;
    private JButton buttonRunSimulation;
    private JLabel labelIterations;
    private JSpinner spinnerIterations;
    private JCheckBox checkBoxShowLineNumbers;
    private JTabbedPane tabbedPaneResults;
    private JPanel panelChart;
    private JPanel panelNumeric;
    private JScrollPane scrollPane2;
    private JTextArea textAreaNumeric;
    private JProgressBar progressBar;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
