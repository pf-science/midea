/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import java.util.concurrent.RecursiveTask;

public class DecoderHamming74 extends RecursiveTask<BinaryNumber[]> implements Decoder
{
    private long extraBitsDecoded = 0;
    private BinaryNumber[] sequence;
    private int lowBound, highBound, sequentialThreshold;

    public DecoderHamming74()
    {

    }

    public DecoderHamming74(BinaryNumber[] _sequence, int _lowBound, int _highBound, int _sequentialThreshold)
    {
        sequence = _sequence;
        lowBound = _lowBound;
        highBound = _highBound;
        sequentialThreshold = _sequentialThreshold;
    }

    @Override
    public BinaryNumber[] compute()
    {
        BinaryNumber[] out = new BinaryNumber[highBound - lowBound];
        if (highBound - lowBound <= sequentialThreshold)
        {
            for (int index = lowBound; index < highBound; ++index)
            {
                boolean[] seq = sequence[index].getData();
                boolean[] syndrome = new boolean[3];
                syndrome[0] = seq[3] ^ seq[4] ^ seq[5] ^ seq[6];
                syndrome[1] = seq[1] ^ seq[2] ^ seq[5] ^ seq[6];
                syndrome[2] = seq[0] ^ seq[2] ^ seq[4] ^ seq[6];
                long errorBit = (new BinaryNumber(syndrome)).toLong();
                if (errorBit != 0)
                    seq[(int)errorBit - 1] = !seq[(int) errorBit - 1];
                boolean[] decoded = new boolean[4];
                System.arraycopy(seq, 0, decoded, 0, 4);
                out[index - lowBound] = new BinaryNumber(decoded);
            }
        } else
        {
            int mid = lowBound + (highBound - lowBound) / 2;
            DecoderHamming74 left  = new DecoderHamming74(sequence, lowBound, mid, sequentialThreshold);
            DecoderHamming74 right = new DecoderHamming74(sequence, mid, highBound, sequentialThreshold);
            left.fork();
            BinaryNumber[] rightAns = right.compute();
            BinaryNumber[] leftAns  = left.join();
            System.arraycopy(leftAns, 0, out, 0, leftAns.length);
            System.arraycopy(rightAns, 0, out, leftAns.length, rightAns.length);
            rightAns = null;
            leftAns = null;
        }
        return out;
    }

    public long getExtraBitsDecoded()
    {
        return extraBitsDecoded;
    }

    public BinaryNumber[] decode(BinaryNumber[] _sequence)
    {
        long originalBlockSize = _sequence[0].getLength();
        Shaper shaper = new Shaper(_sequence, 7);
        BinaryNumber[] shaped = shaper.reshape();
        extraBitsDecoded += shaper.getExtraBits();
        BinaryNumber[] preshaped = WorkersTools.fjPool.invoke(new DecoderHamming74(shaped, 0, shaped.length, WorkersTools.getSequentialThreshold(shaped.length)));
        shaper = new Shaper(preshaped, originalBlockSize);
        BinaryNumber[] ret = shaper.reshape();
        extraBitsDecoded += shaper.getExtraBits();
        return ret;
    }
}
