/*
 * Copyright (C) 2009-2013 Oleksandr Natalenko aka post-factum
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Universal Program License as published by
 * Oleksandr Natalenko aka post-factum; see file COPYING for details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Universal Program
 * License along with this program; if not, write to
 * oleksandr@natalenko.name
 */

package org.pf.midea;

import javax.swing.*;
import java.awt.*;

public class PlanCellRenderer extends JLabel implements ListCellRenderer
{
    public PlanCellRenderer()
    {
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList jList, Object o, int i, boolean b, boolean b1)
    {
        PlanCell planCell = (PlanCell)o;
        setText(planCell.getDescription());
        if (b) {
            setBackground(Color.lightGray);
            setForeground(Color.black);
        } else {
            setBackground(Color.white);
            setForeground(Color.black);
        }
        return this;
    }
}
